<?php

namespace RecipeService\RecipeContext\Enum;

/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 16:49
 */
class Messages
{
    const INVALID_DIFFICULTY_LIMIT = 'invalid limit sent %d, max %d';
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 03.05.17
 * Time: 19:40
 */

namespace RecipeService\RecipeContext\Service;

use RecipeService\RecipeContext\Model\Collection\RecipeCollection;
use RecipeService\RecipeContext\Model\Recipe;
use RecipeService\RecipeContext\Model\RecipePostBody;
use RecipeService\RecipeContext\Model\RequestRecipeArgs;
use RecipeService\RecipeContext\Repository\RecipeRepositoryInterface;
use RecipeService\SharedContext\Exception\NotFoundException;

class RecipeService implements RecipeServiceInterface
{
    /**
     * @var RecipeRepositoryInterface
     */
    private $recipeRepository;

    /**
     * @param RecipeRepositoryInterface $recipeRepository
     */
    public function __construct(RecipeRepositoryInterface $recipeRepository)
    {
        $this->recipeRepository = $recipeRepository;
    }

    /**
     * @inheritdoc
     */
    public function addRecipe(RecipePostBody $recipePostBody): Recipe
    {
        return $this->recipeRepository->addRecipe($recipePostBody);
    }

    /**
     * @inheritdoc
     */
    public function getRecipes(array $query = []): RecipeCollection
    {
        $requestRecipeArgs = new RequestRecipeArgs($query);
        $recipes = $this->recipeRepository->getRecipes($requestRecipeArgs);

        if ($recipes->isEmpty()) {
            throw new NotFoundException();
        }

        return $recipes;
    }

    /**
     * @inheritdoc
     */
    public function getRecipeById($id): Recipe
    {
        return $this->recipeRepository->getRecipeById($id);
    }

    /**
     * @inheritdoc
     */
    public function updateRecipe(int $id, RecipePostBody $postBody): Recipe
    {
        return $this->recipeRepository->updateRecipe($id, $postBody);
    }

    /**
     * @inheritdoc
     */
    public function deleteRecipe(int $id)
    {
        $this->recipeRepository->deleteRecipe($id);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function hasRecipeId(int $id): bool
    {
        try {
            $this->recipeRepository->getRecipeById($id);

            return true;
        } catch (NotFoundException $e) {
            return false;
        }
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 11:25
 */

namespace RecipeService\RecipeContext\Service;

use RecipeService\RecipeContext\Model\Rate;
use RecipeService\RecipeContext\Repository\RateRepositoryInterface;
use RecipeService\SharedContext\Exception\NotFoundException;

class RateService implements RatesServiceInterface
{
    /**
     * @var RecipeServiceInterface
     */
    private $service;
    /**
     * @var RateRepositoryInterface
     */
    private $ratesRepository;

    /**
     * RateService constructor.
     * @param RecipeServiceInterface $service
     * @param RateRepositoryInterface $ratesRepository
     */
    public function __construct(RecipeServiceInterface $service, RateRepositoryInterface $ratesRepository)
    {
        $this->service = $service;
        $this->ratesRepository = $ratesRepository;
    }

    /**
     * @param Rate $rate
     * @throws NotFoundException
     */
    public function rateRecipe(Rate $rate)
    {
        if (!$this->service->hasRecipeId($rate->getRecipeId())) {
            throw new NotFoundException();
        }

        $this->ratesRepository->updateRecipeRate($rate);
    }
}

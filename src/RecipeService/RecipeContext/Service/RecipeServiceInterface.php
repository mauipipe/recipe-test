<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 03.05.17
 * Time: 18:39
 */

namespace RecipeService\RecipeContext\Service;

use RecipeService\RecipeContext\Model\Collection\RecipeCollection;
use RecipeService\RecipeContext\Model\Recipe;
use RecipeService\RecipeContext\Model\RecipePostBody;

interface RecipeServiceInterface
{
    /**
     * @param RecipePostBody $recipePostBody
     * @return Recipe
     */
    public function addRecipe(RecipePostBody $recipePostBody): Recipe;

    /**
     * @param array $query
     * @return RecipeCollection
     */
    public function getRecipes(array $query = []): RecipeCollection;

    /**
     * @param $id
     * @return Recipe
     */
    public function getRecipeById($id): Recipe;

    /**
     * @param int $id
     * @param RecipePostBody $postBody
     * @return Recipe
     */
    public function updateRecipe(int $id, RecipePostBody $postBody): Recipe;

    /**
     * @param $id
     * @return void
     */
    public function deleteRecipe(int $id);

    /**
     * @param int $id
     * @return bool
     */
    public function hasRecipeId(int $id): bool;
}

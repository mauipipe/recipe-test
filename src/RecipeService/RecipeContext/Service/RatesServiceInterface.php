<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 11:20
 */

namespace RecipeService\RecipeContext\Service;

use RecipeService\RecipeContext\Model\Rate;

interface RatesServiceInterface
{

    /**
     * @param Rate $rate
     */
    public function rateRecipe(Rate $rate);
}

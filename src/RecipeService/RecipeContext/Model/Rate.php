<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 11:22
 */

namespace RecipeService\RecipeContext\Model;

class Rate
{
    const RECIPE_ID = 'recipe_id';
    const RATE = 'rate';

    /**
     * @var int
     */
    private $recipeId;
    /**
     * @var int
     */
    private $rate;

    /**
     * Rate constructor.
     * @param int $recipeId
     * @param int $rate
     * @internal param mixed $bodyData
     */
    public function __construct(int $recipeId, int $rate)
    {
        $this->recipeId = $recipeId;
        $this->rate = $rate;
    }

    /**
     * @return int
     */
    public function getRate(): int
    {
        return $this->rate;
    }

    /**
     * @return int
     */
    public function getRecipeId(): int
    {
        return $this->recipeId;
    }
}

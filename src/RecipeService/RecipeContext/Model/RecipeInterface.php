<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 03.05.17
 * Time: 18:51
 */

namespace RecipeService\RecipeContext\Model;

interface RecipeInterface
{

    /**
     * @return int
     */
    public function getPrepTime(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return int
     */
    public function getDifficulty(): int;
}

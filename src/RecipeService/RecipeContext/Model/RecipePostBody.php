<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 03.05.17
 * Time: 18:41
 */

namespace RecipeService\RecipeContext\Model;

class RecipePostBody implements RecipeInterface
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $prepTime;
    /**
     * @var int
     */
    private $difficulty;
    /**
     * @var int
     */
    private $isVegetarian;

    /**
     * Recipe constructor.
     * @param array $postBody
     * @internal param string $name
     * @internal param int $prepTime
     * @internal param bool $isVegetarian
     */
    public function __construct(array $postBody)
    {
        $this->name = $postBody[Recipe::NAME];
        $this->prepTime = $postBody[Recipe::PREP_TIME];
        $this->difficulty = $postBody[Recipe::DIFFICULTY];
        $this->isVegetarian = (int) (("false" === $postBody[Recipe::VEGETARIAN]) ? false : true);
    }

    /**
     * @return int
     */
    public function getDifficulty(): int
    {
        return $this->difficulty;
    }

    /**
     * @inheritdoc
     */
    public function isVegetarian(): int
    {
        return $this->isVegetarian;
    }

    /**
     * @inheritdoc
     */
    public function getPrepTime(): int
    {
        return $this->prepTime;
    }

    /**
     * @inheritdoc
     */
    public function getName(): string
    {
        return $this->name;
    }
}

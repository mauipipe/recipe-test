<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 03.05.17
 * Time: 23:09
 */

namespace RecipeService\RecipeContext\Model;

interface ResponseSerializerInterface
{
    /**
     * @return array
     */
    public function getResponseData(): array;
}

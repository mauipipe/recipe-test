<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 08.05.17
 * Time: 22:28
 */

namespace RecipeService\RecipeContext\Model;

use RecipeService\SharedContext\Exception\UndefinedConfigValueException;

class RequestRecipeArgs
{
    const PAGE = 'page';
    const FIRST_PAGE = 0;
    const DEFAULT_ITEM_PER_PAGE = 20;
    const ITEM_PER_PAGE = 'item_per_page';

    /**
     * @var array
     */
    private $args;
    /**
     * @var int
     */
    private $page;
    /**
     * @var int
     */
    private $itemPerPage;

    private static $paginationParams = [
        self::PAGE,
        self::ITEM_PER_PAGE,
    ];

    /**
     * RequestRecipeArgs constructor.
     * @param array $args
     */
    public function __construct(array $args)
    {
        $this->args = $args;
    }

    /**
     * @throws UndefinedConfigValueException
     */
    public function getQueryParams(): array
    {
        $result = [];
        foreach ($this->args as $arg => $value) {
            if (!in_array($arg, self::$paginationParams)) {
                if (is_numeric($value) || $this->isBool($value)) {
                    $result[$arg] = (int)$value;
                } else {
                    $result[$arg] = $value;
                }
            }
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getFirstResult(): int
    {
        if (!array_key_exists(self::PAGE, $this->args)) {
            return self::FIRST_PAGE;
        }
        if (null === $this->page) {
            $this->page = (int)$this->args[self::PAGE] - 1;
        }

        return $this->page;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        if (!array_key_exists(self::ITEM_PER_PAGE, $this->args)) {
            return self::DEFAULT_ITEM_PER_PAGE;
        }
        if (null === $this->itemPerPage) {
            $this->itemPerPage = $this->args[self::ITEM_PER_PAGE];
        }

        return $this->itemPerPage;
    }

    /**
     * @return bool
     */
    public function hasQueryArgs(): bool
    {
        $args = $this->args;
        foreach (self::$paginationParams as $paginationParam) {
            unset($args[$paginationParam]);
        }

        return count($args) > 0;
    }

    /**
     * @param $string
     * @return bool
     */
    private function isBool($string)
    {
        $string = strtolower($string);

        return (in_array($string, ["true", "false", "1", "0", "yes", "no"], true));
    }
}

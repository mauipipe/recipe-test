<?php

namespace RecipeService\RecipeContext\Model\Collection;

use RecipeService\RecipeContext\Model\Recipe;

/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 03.05.17
 * Time: 22:20
 */
class RecipeCollection
{
    const DEFAULT_COUNT = 0;
    /**
     * @var \ArrayObject|[]Recipe
     */
    private $recipeCollection;
    /**
     * @var int
     */
    private $totalRecipeCount = self::DEFAULT_COUNT;

    /**
     * @param array $recipeData
     */
    public function __construct(array $recipeData = [])
    {
        $this->recipeCollection = new \ArrayObject($recipeData);
    }

    /**
     * @param Recipe $recipe
     */
    public function addRecipe(Recipe $recipe)
    {
        $this->recipeCollection->append($recipe);
    }

    /**
     * @return \ArrayObject
     */
    public function getCollection(): \ArrayObject
    {
        return $this->recipeCollection;
    }

    /**
     * @param int $totalRecipeCount
     */
    public function setTotalRecipeNr(int $totalRecipeCount)
    {
        $this->totalRecipeCount = $totalRecipeCount;
    }

    /**
     * @return int
     */
    public function getTotalRecipeNr(): int
    {
        return $this->totalRecipeCount;
    }

    /**
     * @bool
     */
    public function isEmpty(): bool
    {
        return self::DEFAULT_COUNT === $this->recipeCollection->count();
    }
}

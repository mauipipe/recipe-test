<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 03.05.17
 * Time: 18:44
 */

namespace RecipeService\RecipeContext\Model;

class Recipe implements RecipeInterface
{
    const ID = 'id';
    const VEGETARIAN = 'vegetarian';
    const NAME = 'name';
    const PREP_TIME = 'prep_time';
    const DIFFICULTY = 'difficulty';
    const RATE = 'rate';


    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $prepTime;
    /**
     * @var bool
     */
    private $isVegetarian;
    /**
     * @var int
     */
    private $difficulty;
    /**
     * @var float
     */
    private $rate;

    /**
     * Recipe constructor.
     * @param int $id
     * @param string $name
     * @param int $prepTime
     * @param int $difficulty
     * @param bool $isVegetarian
     * @param float $rate
     */
    public function __construct(
        int $id,
        string $name,
        int $prepTime,
        int $difficulty,
        bool $isVegetarian,
        float $rate
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->prepTime = $prepTime;
        $this->isVegetarian = $isVegetarian;
        $this->difficulty = $difficulty;
        $this->rate = $rate;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return round($this->rate, 1);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isVegetarian(): bool
    {
        return (bool)$this->isVegetarian;
    }

    /**
     * @return int
     */
    public function getPrepTime(): int
    {
        return $this->prepTime;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getDifficulty(): int
    {
        return $this->difficulty;
    }
}

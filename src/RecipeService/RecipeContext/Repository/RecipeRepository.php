<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 03.05.17
 * Time: 20:09
 */

namespace RecipeService\RecipeContext\Repository;

use Doctrine\Common\Proxy\Exception\OutOfBoundsException;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use RecipeService\RecipeContext\Model\Collection\RecipeCollection;
use RecipeService\RecipeContext\Model\Rate;
use RecipeService\RecipeContext\Model\Recipe;
use RecipeService\RecipeContext\Model\RecipePostBody;
use RecipeService\RecipeContext\Model\RequestRecipeArgs;
use RecipeService\SharedContext\Exception\NotFoundException;

class RecipeRepository implements RecipeRepositoryInterface
{
    const RECIPE_ID_SEQ = "recipes_id_seq";
    const RECIPES_TABLE = "recipes";
    const WHERE = '%s.%s=?';
    const PLACEHOLDER = "?";
    const REL_QUERY = '%s.%s';
    const INITIAL_RATE_SCORE = 0.0;

    /**
     * @var QueryBuilder
     */
    private $queryBuilder;
    /**
     * @var Connection
     */
    private $connection;

    /**
     * RecipeRepository constructor.
     * @param QueryBuilder $queryBuilder
     * @param Connection $connection
     */
    public function __construct(QueryBuilder $queryBuilder, Connection $connection)
    {
        $this->queryBuilder = $queryBuilder;
        $this->connection = $connection;
    }

    /**
     * @inheritdoc
     */
    public function addRecipe(RecipePostBody $recipePostBody): Recipe
    {
        $query = $this->queryBuilder->insert(self::RECIPES_TABLE)
            ->setValue(Recipe::NAME, self::PLACEHOLDER)
            ->setValue(Recipe::PREP_TIME, self::PLACEHOLDER)
            ->setValue(Recipe::DIFFICULTY, self::PLACEHOLDER)
            ->setValue(Recipe::VEGETARIAN, self::PLACEHOLDER)
            ->setParameter(0, $recipePostBody->getName())
            ->setParameter(1, $recipePostBody->getPrepTime())
            ->setParameter(2, $recipePostBody->getDifficulty())
            ->setParameter(3, $recipePostBody->isVegetarian());


        $query->execute();

        $recipeId = $this->connection->lastInsertId();

        return $this->createRecipeFromRequestBody($recipeId, $recipePostBody);
    }

    /**
     * @inheritdoc
     */
    public function getRecipes(RequestRecipeArgs $queryArgs): RecipeCollection
    {
        $totalRecipeNr = $this->getTotalRecipeNr();
        $offset = $queryArgs->getOffset();
        $firstResult = $queryArgs->getFirstResult();
        $query = $this->createSelectRecipeQuery();

        if ($queryArgs->hasQueryArgs()) {
            $query = $this->createWhereConditionFromArgs($queryArgs->getQueryParams(), $query);
        }
        $aliasRecipe = substr(self::RECIPES_TABLE, 0, 1);

        $query->groupBy(sprintf(self::REL_QUERY, $aliasRecipe, Recipe::ID));
        $query->setFirstResult($firstResult)
            ->setMaxResults($offset);

        $result = $query->execute();

        $recipeCollection = new RecipeCollection();
        $recipeCollection->setTotalRecipeNr($totalRecipeNr);

        foreach ($result->fetchAll() as $value) {
            $recipe = $this->createRecipe($value);
            $recipeCollection->addRecipe($recipe);
        }

        return $recipeCollection;
    }

    /**
     * @inheritdoc
     */
    public function getRecipeById(int $id): Recipe
    {
        $query = $this->createSelectRecipeQuery();
        $aliasRecipe = substr(self::RECIPES_TABLE, 0, 1);
        $query->where(sprintf(self::WHERE, $aliasRecipe, Recipe::ID))
            ->setParameter(0, $id);

        $query->groupBy(sprintf(self::REL_QUERY, $aliasRecipe, Recipe::ID));

        $result = $query->execute();

        $recipeData = $result->fetch();

        if (empty($recipeData)) {
            throw new NotFoundException();
        }

        return $this->createRecipe($recipeData);
    }

    /**
     * @inheritdoc
     */
    public function updateRecipe(int $id, RecipePostBody $recipePostBody): Recipe
    {
        $query = $this->queryBuilder->update(self::RECIPES_TABLE)
            ->set(Recipe::NAME, self::PLACEHOLDER)
            ->set(Recipe::PREP_TIME, self::PLACEHOLDER)
            ->set(Recipe::DIFFICULTY, self::PLACEHOLDER)
            ->set(Recipe::VEGETARIAN, self::PLACEHOLDER)
            ->setParameter(0, $recipePostBody->getName())
            ->setParameter(1, $recipePostBody->getPrepTime())
            ->setParameter(2, $recipePostBody->getDifficulty())
            ->setParameter(3, $recipePostBody->isVegetarian())
            ->setParameter(4, (int)$id)
            ->where(sprintf(self::WHERE, self::RECIPES_TABLE, Recipe::ID));

        $query->execute();

        return $this->createRecipeFromRequestBody($id, $recipePostBody);
    }

    /**
     * @inheritdoc
     */
    public function deleteRecipe(int $id)
    {
        $query = $this->queryBuilder->delete(self::RECIPES_TABLE)
            ->where(sprintf(self::WHERE, self::RECIPES_TABLE, Recipe::ID))
            ->setParameter(0, $id);

        $query->execute();
    }

    /**
     * @param int $recipeId
     * @param RecipePostBody $recipePostBody
     * @return Recipe
     */
    private function createRecipeFromRequestBody(int $recipeId, RecipePostBody $recipePostBody): Recipe
    {
        return new Recipe(
            $recipeId,
            $recipePostBody->getName(),
            $recipePostBody->getPrepTime(),
            $recipePostBody->getDifficulty(),
            $recipePostBody->isVegetarian(),
            self::INITIAL_RATE_SCORE
        );
    }


    /**
     * @param array $value
     * @return Recipe
     */
    private function createRecipe(array $value): Recipe
    {
        $rate = isset($value[Recipe::RATE]) ? $value[Recipe::RATE] : self::INITIAL_RATE_SCORE;

        return new Recipe(
            $value[Recipe::ID],
            $value[Recipe::NAME],
            $value[Recipe::PREP_TIME],
            $value[Recipe::DIFFICULTY],
            $value[Recipe::VEGETARIAN],
            $rate
        );
    }

    /**
     * @param array $queryArgs
     * @param $query
     * @return mixed
     */
    private function createWhereConditionFromArgs(array $queryArgs, QueryBuilder $query)
    {
        $key = 0;
        $whereQuery = $this->queryBuilder->expr()->andX();
        $alias = $this->getAliases(self::RECIPES_TABLE);
        foreach ($queryArgs as $arg => $value) {
            $queryArg = sprintf(self::REL_QUERY, $alias, $arg);
            $expression = $this->queryBuilder->expr()->eq($queryArg, '?');
            if ($arg === Recipe::NAME) {
                $expression = $this->queryBuilder->expr()->like($queryArg, '?');
            }
            $whereQuery->add($expression);
            $query->setParameter($key, $value);
            if ($arg === Recipe::NAME) {
                $query->setParameter($key, $value.'%');
            }
        }

        return $query->where($whereQuery);
    }

    /**
     * @return QueryBuilder
     */
    private function createSelectRecipeQuery(): QueryBuilder
    {
        $aliasRecipe = $this->getAliases(self::RECIPES_TABLE);
        $aliasRates = $this->getAliases(RateRepository::RATES_TABLE);

        return $this->queryBuilder->select(
            sprintf(self::REL_QUERY, $aliasRecipe, Recipe::ID),
            sprintf(self::REL_QUERY, $aliasRecipe, Recipe::NAME),
            sprintf(self::REL_QUERY, $aliasRecipe, Recipe::PREP_TIME),
            sprintf(self::REL_QUERY, $aliasRecipe, Recipe::DIFFICULTY),
            sprintf(self::REL_QUERY, $aliasRecipe, Recipe::VEGETARIAN),
            sprintf("avg(%s.%s) as %s", $aliasRates, Rate::RATE, Recipe::RATE)
        )
            ->from(self::RECIPES_TABLE, $aliasRecipe)
            ->leftjoin(
                $aliasRecipe,
                RateRepository::RATES_TABLE,
                $aliasRates,
                sprintf('%s.%s=%s.%s', $aliasRecipe, Recipe::ID, $aliasRates, Rate::RECIPE_ID)
            );
    }

    /**
     * @return int
     */
    private function getTotalRecipeNr(): int
    {
        $query = $this->queryBuilder->select('count(recipes.id) as recipe_nr')
            ->from(self::RECIPES_TABLE);

        $result = $query->execute()->fetch();

        return $result['recipe_nr'];
    }

    /**
     * @param string $table
     * @return bool|string
     */
    private function getAliases(string $table)
    {
        switch ($table) {
            case self::RECIPES_TABLE:
                return substr(self::RECIPES_TABLE, 0, 1);
            case RateRepository::RATES_TABLE:
                return substr(RateRepository::RATES_TABLE, 0, 2);
            default:
                throw new OutOfBoundsException(sprintf('invalid table %s', $table));
                break;
        }
    }
}

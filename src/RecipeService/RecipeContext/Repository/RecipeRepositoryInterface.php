<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 03.05.17
 * Time: 19:46
 */

namespace RecipeService\RecipeContext\Repository;

use RecipeService\RecipeContext\Model\Collection\RecipeCollection;
use RecipeService\RecipeContext\Model\Recipe;
use RecipeService\RecipeContext\Model\RecipePostBody;
use RecipeService\RecipeContext\Model\RequestRecipeArgs;

interface RecipeRepositoryInterface
{
    /**
     * @param RecipePostBody $recipePostBody
     * @return Recipe
     */
    public function addRecipe(RecipePostBody $recipePostBody): Recipe;

    /**
     * @param RequestRecipeArgs $recipeArgs
     * @return RecipeCollection
     * @internal param array $query
     */
    public function getRecipes(RequestRecipeArgs $recipeArgs): RecipeCollection;

    /**
     * @param $id
     * @return Recipe
     */
    public function getRecipeById(int $id): Recipe;

    /**
     * @param $id
     * @param $recipePostBody
     * @return Recipe
     */
    public function updateRecipe(int $id, RecipePostBody $recipePostBody): Recipe;

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteRecipe(int $id);
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 11:58
 */

namespace RecipeService\RecipeContext\Repository;

use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use RecipeService\RecipeContext\Model\Rate;

class RateRepository implements RateRepositoryInterface
{
    const RATES_TABLE = 'rates';
    const RATES_ID_SEQ = 'rates_id_seq';

    /**
     * @var QueryBuilder
     */
    private $queryBuilder;
    /**
     * @var Connection
     */
    private $connection;

    /**
     * RecipeRepository constructor.
     * @param QueryBuilder $queryBuilder
     * @param Connection $connection
     */
    public function __construct(QueryBuilder $queryBuilder, Connection $connection)
    {
        $this->queryBuilder = $queryBuilder;
        $this->connection = $connection;
    }

    /**
     * @param Rate $rate
     */
    public function updateRecipeRate(Rate $rate)
    {
        $query = $this->queryBuilder->insert(self::RATES_TABLE)
            ->setValue(Rate::RECIPE_ID, '?')
            ->setValue(Rate::RATE, '?')
            ->setParameter(0, $rate->getRecipeId())
            ->setParameter(1, $rate->getRate());

        $query->execute();
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 11:36
 */

namespace RecipeService\RecipeContext\Repository;

use RecipeService\RecipeContext\Model\Rate;

interface RateRepositoryInterface
{

    /**
     * @param Rate $rate
     */
    public function updateRecipeRate(Rate $rate);
}

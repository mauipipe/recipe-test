<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 08.05.17
 * Time: 18:53
 */

namespace RecipeService\RecipeContext\Serializer;

use RecipeService\RecipeContext\Model\Collection\RecipeCollection;
use RecipeService\RecipeContext\Model\Recipe;

class RecipeResponseSerializer
{
    const DATA = 'data';
    const TOTAL_RECIPES = 'total_recipes';
    const CURRENT_PAGE = 'current_page';

    /**
     * @param Recipe $recipe
     * @return array
     */
    public static function serialize(Recipe $recipe)
    {
        return [
            Recipe::ID         => $recipe->getId(),
            Recipe::NAME       => $recipe->getName(),
            Recipe::PREP_TIME  => $recipe->getPrepTime(),
            Recipe::DIFFICULTY => $recipe->getDifficulty(),
            Recipe::VEGETARIAN => $recipe->isVegetarian(),
            Recipe::RATE       => $recipe->getRate(),
        ];
    }

    /**
     * @param RecipeCollection $recipeCollection
     * @return array
     */
    public static function serializeCollection(RecipeCollection $recipeCollection)
    {
        $result = [];
        foreach ($recipeCollection->getCollection() as $value) {
            $result[] = static::serialize($value);
        }
        $result = [
            self::DATA          => $result,
            self::TOTAL_RECIPES => $recipeCollection->getTotalRecipeNr(),
        ];

        return $result;
    }
}

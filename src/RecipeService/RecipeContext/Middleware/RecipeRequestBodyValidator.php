<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 05.05.17
 * Time: 19:34
 */

namespace RecipeService\RecipeContext\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use RecipeService\RecipeContext\Exception\InvalidRecipeDifficultyException;
use RecipeService\RecipeContext\Model\Recipe;
use RecipeService\SharedContext\Exception\BadRequestException;
use RecipeService\SharedContext\Middleware\MiddlewareInterface;

class RecipeRequestBodyValidator implements MiddlewareInterface
{
    private static $mandatoryParams = [
        Recipe::NAME,
        Recipe::PREP_TIME,
        Recipe::VEGETARIAN,
        Recipe::DIFFICULTY,
    ];
    const DIFFICULTY_LIMIT = 5;

    /**
     * @inheritdoc
     */
    public function isValid(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ): ResponseInterface {
        $body = json_decode($request->getBody()->getContents(), true);

        if (null === $body) {
            throw new BadRequestException();
        }

        $this->validateBodyNameParams($body);

        $difficulty = $body[Recipe::DIFFICULTY];
        if (self::DIFFICULTY_LIMIT <= $difficulty) {
            throw new InvalidRecipeDifficultyException($difficulty, self::DIFFICULTY_LIMIT);
        }

        $response = $next($request, $response);

        return $response;
    }

    /**
     * @param $body
     * @throws BadRequestException
     */
    protected function validateBodyNameParams(array $body)
    {
        $bodyParamName = array_keys($body);
        $paramDiff = array_diff($bodyParamName, self::$mandatoryParams);
        if (0 < count($paramDiff)) {
            throw new BadRequestException();
        }
    }
}

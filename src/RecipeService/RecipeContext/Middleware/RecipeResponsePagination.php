<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 09.05.17
 * Time: 00:42
 */

namespace RecipeService\RecipeContext\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use RecipeService\RecipeContext\Model\RequestRecipeArgs;
use RecipeService\RecipeContext\Serializer\RecipeResponseSerializer;
use RecipeService\SharedContext\Middleware\MiddlewareInterface;
use Zend\Diactoros\Response;

class RecipeResponsePagination implements MiddlewareInterface
{

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable $next
     * @return ResponseInterface
     */
    public function isValid(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ): ResponseInterface {
        $serverParams = $request->getServerParams();
        /** @var ResponseInterface $response */
        $response = $next($request, $response);

        $queryParams = $request->getQueryParams();
        if (!isset($queryParams[RequestRecipeArgs::PAGE])) {
            return $response;
        }

        $body = json_decode($response->getBody(), true);
        $itemPerPage = $body[RecipeResponseSerializer::TOTAL_RECIPES];
        $page = $queryParams[RequestRecipeArgs::PAGE];

        $body['links'] = [];
        if ($page > 1) {
            $body['links'][] = $this->createLinkUrl($serverParams, $page - 1);
        }
        if ($page < $itemPerPage) {
            $body['links'][] = $this->createLinkUrl($serverParams, $page + 1);
        }

        $response = new Response();
        $response->getBody()->write(json_encode($body));

        return $response->withHeader('Content-Type', 'application/json')
            ->withStatus($response->getStatusCode());
    }

    /**
     * @param $serverParams
     * @param $page
     * @return string
     */
    private function createLinkUrl($serverParams, $page): string
    {
        return sprintf('http://%s/recipes?page=%d', $serverParams['HTTP_HOST'], $page);
    }
}

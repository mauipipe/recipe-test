<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 16:40
 */

namespace RecipeService\RecipeContext\Exception;

use League\Route\Http\Exception;
use RecipeService\RecipeContext\Enum\Messages;
use RecipeService\SharedContext\Enum\StatusCodes;

class InvalidRecipeDifficultyException extends Exception
{
    /**
     * InvalidRecipeDifficultyException constructor.
     * @param int $limitSent
     * @param int $difficultyLimit
     */
    public function __construct(int $limitSent, int $difficultyLimit)
    {
        parent::__construct(
            StatusCodes::BAD_REQUEST,
            sprintf(Messages::INVALID_DIFFICULTY_LIMIT, $limitSent, $difficultyLimit),
            null,
            ['Content-Type' => 'application-json'],
            StatusCodes::BAD_REQUEST
        );
    }
}

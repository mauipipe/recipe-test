<?php

namespace RecipeService\RecipeContext\Controller;

use Doctrine\DBAL\DBALException;
use Psr\Http\Message\ResponseInterface;
use RecipeService\RecipeContext\Model\RecipePostBody;
use RecipeService\RecipeContext\Serializer\RecipeResponseSerializer;
use RecipeService\RecipeContext\Service\RecipeServiceInterface;
use RecipeService\SharedContext\Enum\StatusCodes;
use RecipeService\SharedContext\Exception\InternalServerException;
use RecipeService\SharedContext\Router\RouterInterface;
use Zend\Diactoros\ServerRequest;

/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 02.05.17
 * Time: 23:05
 */
class RecipeController implements RouterInterface
{
    /**
     * @var RecipeServiceInterface
     */
    private $recipeService;

    /**
     * RecipeController constructor.
     * @param RecipeServiceInterface $recipeService
     */
    public function __construct(RecipeServiceInterface $recipeService)
    {
        $this->recipeService = $recipeService;
    }

    /**
     * @param ServerRequest $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     * @throws InternalServerException
     */
    public function addRecipe(ServerRequest $request, ResponseInterface $response, array $args)
    {
        try {
            $bodyData = json_decode($request->getBody()->getContents(), true);
            $postBody = new RecipePostBody($bodyData);
            $recipe = $this->recipeService->addRecipe($postBody);

            $responseData = RecipeResponseSerializer::serialize($recipe);
            $response->getBody()->write(json_encode(['data' => $responseData]));

            return $response->withStatus(StatusCodes::STATUS_CREATED);
        } catch (DBALException $e) {
            throw new InternalServerException($e);
        } catch (\Exception $e) {
            throw new InternalServerException($e);
        }
    }

    /**
     * @param ServerRequest $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     *
     * @throws InternalServerException
     */
    public function updateRecipe(ServerRequest $request, ResponseInterface $response, array $args)
    {
        try {
            $bodyData = json_decode($request->getBody()->getContents(), true);
            $postBody = new RecipePostBody($bodyData);
            $this->recipeService->updateRecipe($args['id'], $postBody);

            return $response->withStatus(StatusCodes::STATUS_NO_CONTENT);
        } catch (DBALException $e) {
            throw new InternalServerException($e);
        } catch (\Exception $e) {
            throw new InternalServerException($e);
        }
    }

    /**
     * @param ServerRequest $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     *
     * @throws InternalServerException
     */
    public function getRecipes(ServerRequest $request, ResponseInterface $response, array $args)
    {
        try {
            $query = $request->getQueryParams();

            $recipes = $this->recipeService->getRecipes($query);

            $recipesResponse = RecipeResponseSerializer::serializeCollection($recipes);
            $response->getBody()->write(json_encode($recipesResponse));

            return $response;
        } catch (DBALException $e) {
            throw new InternalServerException($e);
        } catch (Error$e) {
            throw new InternalServerException($e);
        }
    }

    /**
     * @param ServerRequest $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     *
     * @throws InternalServerException
     */
    public function getRecipeById(ServerRequest $request, ResponseInterface $response, array $args)
    {
        try {
            $recipe = $this->recipeService->getRecipeById($args['id']);

            $recipeResponse = RecipeResponseSerializer::serialize($recipe);
            $response->getBody()->write(json_encode(['data' => $recipeResponse]));

            return $response;
        } catch (DBALException $e) {
            throw new InternalServerException($e);
        } catch (Error$e) {
            throw new InternalServerException($e);
        }
    }

    /**
     * @param ServerRequest $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     *
     * @throws InternalServerException
     */
    public function deleteRecipe(ServerRequest $request, ResponseInterface $response, array $args)
    {
        try {
            $this->recipeService->deleteRecipe($args['id']);

            $response->getBody()->write(json_encode(['']));

            return $response->withStatus(StatusCodes::STATUS_NO_CONTENT);
        } catch (DBALException $e) {
            throw new InternalServerException($e);
        } catch (Error$e) {
            throw new InternalServerException($e);
        }
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 11:12
 */

namespace RecipeService\RecipeContext\Controller;

use Doctrine\DBAL\DBALException;
use Psr\Http\Message\ResponseInterface;
use RecipeService\RecipeContext\Model\Rate;
use RecipeService\RecipeContext\Service\RatesServiceInterface;
use RecipeService\SharedContext\Enum\StatusCodes;
use RecipeService\SharedContext\Exception\InternalServerException;
use RecipeService\SharedContext\Router\RouterInterface;
use Zend\Diactoros\ServerRequest;

class RatesController implements RouterInterface
{
    /**
     * @var RatesServiceInterface
     */
    private $ratesService;

    /**
     * @param RatesServiceInterface $ratesService
     */
    public function __construct(RatesServiceInterface $ratesService)
    {
        $this->ratesService = $ratesService;
    }

    /**
     * @param ServerRequest $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     *
     * @throws InternalServerException
     */
    public function rateRecipe(ServerRequest $request, ResponseInterface $response, array $args)
    {
        try {
            $bodyData = json_decode($request->getBody()->getContents(), true);
            $rate = new Rate($args['id'], $bodyData['rate']);
            $this->ratesService->rateRecipe($rate);

            return $response->withStatus(StatusCodes::STATUS_NO_CONTENT);
        } catch (DBALException $e) {
            throw new InternalServerException($e);
        } catch (\Error $e) {
            throw new InternalServerException($e);
        }
    }
}

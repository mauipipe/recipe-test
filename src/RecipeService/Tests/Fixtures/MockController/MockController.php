<?php

namespace RecipeService\Tests\Fixtures\MockController;

use RecipeService\SharedContext\Router\RouterInterface;

/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 07.05.17
 * Time: 17:02
 */
class MockController implements RouterInterface
{
    public function mockAction()
    {
    }
}

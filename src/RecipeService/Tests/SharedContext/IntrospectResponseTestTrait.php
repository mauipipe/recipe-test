<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 07.05.17
 * Time: 14:56
 */

namespace RecipeService\Tests\SharedContext;

use RecipeService\SharedContext\Model\IntrospectResponse;

trait IntrospectResponseTestTrait
{
    /**
     * @param $body
     * @return IntrospectResponse
     */
    protected function createIntrospectResponse($body): IntrospectResponse
    {
        $decodedBody = json_decode($body);
        $expectedResult = new IntrospectResponse(
            $decodedBody->active,
            $decodedBody->scope,
            $decodedBody->client_id,
            $decodedBody->username,
            $decodedBody->exp
        );

        return $expectedResult;
    }
}

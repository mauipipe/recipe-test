<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 02.05.17
 * Time: 22:23
 */

namespace RecipeService\Test\SharedContext\Router;

use League\Route\Route;
use League\Container\Container as RouteContainer;
use League\Route\Strategy\JsonStrategy;
use Pimple\Container;
use RecipeService\RecipeContext\Middleware\RecipeRequestBodyValidator;
use RecipeService\SharedContext\Router\Router;
use RecipeService\Tests\Fixtures\MockController\MockController;
use Zend\Diactoros\Response\SapiEmitter;
use League\Route\RouteCollection;

class RouterTest extends \PHPUnit_Framework_TestCase
{
    const HANDLER_NAME_TEST = 'handler_name_test';
    const IS_VALID = 'isValid';

    /**
     * @var RouteCollection|\PHPUnit_Framework_MockObject_MockObject
     */
    private $routeCollection;
    /**
     * @var Route|\PHPUnit_Framework_MockObject_MockObject
     */
    private $route;
    /**
     * @var Container|\PHPUnit_Framework_MockObject_MockObject
     */
    private $container;
    /**
     * @var RouteContainer|\PHPUnit_Framework_MockObject_MockObject
     */
    private $routeContainer;
    /**
     * @var Router
     */
    private $router;

    public function setUp()
    {
        $this->routeContainer = $this->getMockBuilder('League\Container\Container')
            ->disableOriginalConstructor()
            ->getMock();
        $this->container = $this->getMockBuilder('Pimple\Container')
            ->disableOriginalConstructor()
            ->getMock();
        $this->routeCollection = $this->getMockBuilder('League\Route\RouteCollection')
            ->disableOriginalConstructor()
            ->getMock();
        $this->route = $this->getMockBuilder('League\Route\Route')
            ->disableOriginalConstructor()
            ->getMock();


        $this->router = new Router($this->routeContainer, $this->container, $this->routeCollection);
    }

    /**
     * @test
     * @expectedException \RecipeService\SharedContext\Exception\HandlerNotFoundException
     */
    public function throwsExceptionWhenInvalidHandlerRetrieved()
    {
        $this->prepareEmitter();

        $this->container->expects($this->once())
            ->method('offsetGet')
            ->with(self::HANDLER_NAME_TEST)
            ->willReturn(null);

        $testRoutes = [
            [
                "method"  => "GET",
                "url"     => "/recipes",
                "handler" => self::HANDLER_NAME_TEST,
                "action"  => "getRecipes",
            ],
        ];

        $this->router->initRoutes(new JsonStrategy(), $testRoutes);
    }

    /**
     * @test
     */
    public function addSuccessFullyRouteWhenNoExceptionIsThrown()
    {
        $testRoutes = [
            [
                "method"  => "GET",
                "url"     => "/recipes",
                "handler" => MockController::class,
                "action"  => "mockAction",
            ],
        ];

        $this->prepareEmitter();
        $strategy = new JsonStrategy();

        $this->container->expects($this->once())
            ->method('offsetGet')
            ->with(MockController::class)
            ->willReturn(new MockController());

        $this->routeCollection->expects($this->once())
            ->method('setStrategy')
            ->with($strategy);

        $this->routeCollection->expects($this->any())
            ->method('map')
            ->with(
                $testRoutes[0]['method'],
                $testRoutes[0]['url'],
                $this->anything()
            );

        $this->router->initRoutes($strategy, $testRoutes);
    }

    /**
     * @test
     */
    public function addSuccessFullyRouteWithMiddlewareWhenNoExceptionIsThrown()
    {
        $middleware = [
            [RecipeRequestBodyValidator::class, self::IS_VALID],
            //[AuthenticationMiddleware::class, 'isValid'],
        ];
        $testRoutes = [
            [
                "method"     => "GET",
                "url"        => "/recipes",
                "handler"    => MockController::class,
                "action"     => "mockAction",
                "middleware" => $middleware,
            ],
        ];

        $this->prepareEmitter();
        $strategy = new JsonStrategy();
        $middlewareExample = new RecipeRequestBodyValidator();

        $this->container->expects($this->at(0))
            ->method('offsetGet')
            ->with(MockController::class)
            ->willReturn(new MockController());

        $this->container->expects($this->at(1))
            ->method('offsetGet')
            ->with(RecipeRequestBodyValidator::class)
            ->willReturn($middlewareExample);

        $this->route->expects($this->once())
            ->method('middleware')
            ->with([$middlewareExample, self::IS_VALID]);

        $this->routeCollection->expects($this->once())
            ->method('setStrategy')
            ->with($strategy);

        $this->routeCollection->expects($this->any())
            ->method('map')
            ->with(
                $testRoutes[0]['method'],
                $testRoutes[0]['url'],
                $this->anything()
            )->willReturn($this->route);

        $this->router->initRoutes($strategy, $testRoutes);
    }

    /**
     * @test
     * @expectedException \RecipeService\SharedContext\Exception\UndefinedConfigValueException
     */
    public function throwsExceptionWhenConfigKeyMissing()
    {
        $this->prepareEmitter();


        $testRoutes = [
            [
                "method" => "GET",
                "url"    => "/recipes",
                "action" => "getRecipes",
            ],
        ];

        $this->router->initRoutes(new JsonStrategy(), $testRoutes);
    }

    private function prepareEmitter()
    {
        $this->routeContainer->expects($this->once())
            ->method('share')
            ->with(Router::EMITTER, SapiEmitter::class);
    }
}

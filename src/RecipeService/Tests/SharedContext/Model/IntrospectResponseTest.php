<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 07.05.17
 * Time: 14:52
 */

namespace RecipeService\Test\SharedContext\Model;

use RecipeService\Tests\SharedContext\IntrospectResponseTestTrait;

class IntrospectResponseTest extends \PHPUnit_Framework_TestCase
{
    use IntrospectResponseTestTrait;

    const SECOND = 1000;

    /**
     * @test
     */
    public function isValidIntrospectResponseWhenExpirationActiveAndScopeAreMatching()
    {
        $body = '
        {
          "active": true,
          "scope": "read",
          "client_id": "J8NFmU4tJVgDxKaJFmXTWvaHO",
          "username": "test",
          "exp":0
        }';
        $scopes = 'read, write';

        $introspectToken = $this->createIntrospectResponse($body);
        $currentTime = time();
        $introspectToken->validate($scopes, $currentTime);
    }

    /**
     * @test
     * @expectedException \RecipeService\SharedContext\Exception\NotAuthorizedException
     *
     * @dataProvider bodyProvider
     * @param string $body
     * @param string $scopes
     * @param int $currentTime
     */
    public function throwNotAuthorizedExceptionWhenExpirationActiveOrScopeNotMatching(
        string $body,
        string $scopes,
        int $currentTime
    ) {
        $introspectToken = $this->createIntrospectResponse($body);
        $introspectToken->validate($scopes, $currentTime);
    }

    /**
     * @return array
     */
    public function bodyProvider()
    {
        $currentTime = time();

        return [
            // Not active
            [
                sprintf('
                {
                  "active": false,
                  "scope": "read",
                  "client_id": "J8NFmU4tJVgDxKaJFmXTWvaHO",
                  "username": "test",
                  "exp":%d
                }', 0),
                'read, write',
                time(),
            ],
            // Out of scopes
            [
                sprintf('
                {
                  "active": true,
                  "scope": "write",
                  "client_id": "J8NFmU4tJVgDxKaJFmXTWvaHO",
                  "username": "test",
                  "exp":%d
                }', 0),
                'read',
                time(),
            ],
            // Expired
            [
                sprintf('
                {
                  "active": true,
                  "scope": "write",
                  "client_id": "J8NFmU4tJVgDxKaJFmXTWvaHO",
                  "username": "test",
                  "exp":%d
                }', $currentTime - self::SECOND),
                'read',
                $currentTime,
            ],
        ];
    }
}

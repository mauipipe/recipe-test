<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 19:08
 */

namespace RecipeService\Tests\SharedContext\Auth;

use GuzzleHttp\Psr7\Response;
use RecipeService\SharedContext\Auth\JwtAuthAdapter;
use GuzzleHttp\Client;
use RecipeService\SharedContext\Enum\InternalEndpoints;
use RecipeService\Tests\SharedContext\AbstractHttpTest;
use RecipeService\Tests\SharedContext\IntrospectResponseTestTrait;

class JwtAuthAdapterTest extends AbstractHttpTest
{
    use IntrospectResponseTestTrait;

    /**
     * @var Client|\PHPUnit_Framework_MockObject_MockObject
     */
    private $client;
    /**
     * @var JwtAuthAdapter;
     */
    private $jwtAuthAdapter;

    public function setUp()
    {
        parent::setUp();
        $this->client = $this->getMockBuilder('GuzzleHttp\Client')
            ->setMethods(['post'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->jwtAuthAdapter = new JwtAuthAdapter($this->client);
    }

    /**
     * @test
     * @expectedException \RecipeService\SharedContext\Exception\NotAuthorizedException
     */
    public function throwsNotAuthorizedWhenResponseIntrospectionIsInValidJson()
    {
        $invalidToken = 'invalid';

        $this->client->expects($this->once())
            ->method('post')
            ->with(InternalEndpoints::TOKEN_INTROSPECT,
                [
                    'headers' => ['content-type' => JwtAuthAdapter::APPLICATION_JSON],
                    'body'    => json_encode(['token' => $invalidToken]),
                ]
            )->willReturn(new Response());

        $this->jwtAuthAdapter->introspectToken($invalidToken);
    }

    /**
     * @test
     * @expectedException \RecipeService\SharedContext\Exception\NotAuthorizedException
     */
    public function throwsNotAuthorizedWhenIntrospectResponseNotContainsMandatoryParams()
    {
        $invalidToken = 'invalid';

        $body = '
        {
          "active": true,
          "scope": "read",
          "client_id": "J8NFmU4tJVgDxKaJFmXTWvaHO",
          "username": "test"
        }';

        $this->prepareMockResponse($body);


        $this->client->expects($this->once())
            ->method('post')
            ->with(InternalEndpoints::TOKEN_INTROSPECT,
                [
                    'headers' => ['content-type' => JwtAuthAdapter::APPLICATION_JSON],
                    'body'    => json_encode(['token' => $invalidToken]),
                ]
            )->willReturn($this->response);

        $this->jwtAuthAdapter->introspectToken($invalidToken);
    }

    /**
     * @test
     */
    public function returnsIntrospectResponse()
    {
        $invalidToken = 'invalid';

        $body = '
        {
          "active": true,
          "scope": "read",
          "client_id": "J8NFmU4tJVgDxKaJFmXTWvaHO",
          "username": "test",
          "exp":0
        }';


        $this->prepareMockResponse($body);


        $this->client->expects($this->once())
            ->method('post')
            ->with(InternalEndpoints::TOKEN_INTROSPECT,
                [
                    'headers' => ['content-type' => JwtAuthAdapter::APPLICATION_JSON],
                    'body'    => json_encode(['token' => $invalidToken]),
                ]
            )->willReturn($this->response);

        $expectedResult = $this->createIntrospectResponse($body);

        $result = $this->jwtAuthAdapter->introspectToken($invalidToken);

        $this->assertEquals($expectedResult, $result);
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 07.05.17
 * Time: 15:16
 */

namespace RecipeService\Tests\SharedContext\Middleware;

use GuzzleHttp\Exception\ClientException;
use RecipeService\SharedContext\Auth\JwtAuthAdapter;
use RecipeService\SharedContext\Exception\NotAuthorizedException;
use RecipeService\SharedContext\Middleware\AuthenticationMiddleware;
use RecipeService\Tests\SharedContext\AbstractHttpTest;
use RecipeService\Tests\SharedContext\IntrospectResponseTestTrait;

class AuthenticationMiddlewareTest extends AbstractHttpTest
{
    use IntrospectResponseTestTrait;

    /**
     * @var JwtAuthAdapter|\PHPUnit_Framework_MockObject_MockObject
     */
    private $jwtAdapter;

    /**
     * @var AuthenticationMiddleware
     */
    private $authenticationMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->jwtAdapter = $this->getMockBuilder('RecipeService\SharedContext\Auth\JwtAuthAdapter')
            ->disableOriginalConstructor()
            ->getMock();

        $this->authenticationMiddleware = new AuthenticationMiddleware($this->jwtAdapter, 'read,write');
    }

    /**
     * @test
     * @expectedException \RecipeService\SharedContext\Exception\NotAuthorizedException
     * @dataProvider headerProvider
     */
    public function throwsNotAuthorizedExceptionWhenHeaderNotValid($header)
    {
        $this->request->expects($this->once())
            ->method('getHeader')
            ->with(AuthenticationMiddleware::HEADER_KEY)
            ->willReturn($header);

        $this->authenticationMiddleware->isValid(
            $this->request,
            $this->response,
            function () {
            }
        );
    }

    /**
     * @return array
     */
    public function headerProvider()
    {
        return [
            [null],
            [['']],
            [[sprintf('%s:', AuthenticationMiddleware::BEARER)]],
            [[sprintf('%s', AuthenticationMiddleware::BEARER)]],
        ];
    }

    /**
     * @test
     * @expectedException \RecipeService\SharedContext\Exception\NotAuthorizedException
     */
    public function throwsNotAuthorizedExceptionWhenCallOrResponseNotValid()
    {
        $token = '12312313';
        $header = sprintf('%s: %s', AuthenticationMiddleware::BEARER, $token);
        $this->request->expects($this->once())
            ->method('getHeader')
            ->with(AuthenticationMiddleware::HEADER_KEY)
            ->willReturn([$header]);

        $this->jwtAdapter->expects($this->once())
            ->method('introspectToken')
            ->with($token)
            ->willThrowException(new NotAuthorizedException());

        $this->authenticationMiddleware->isValid(
            $this->request,
            $this->response,
            function () {
            }
        );
    }

    /**
     * @test
     * @expectedException \RecipeService\SharedContext\Exception\NotAuthorizedException
     */
    public function throwsNotAuthorizedExceptionWhenInvalidIntrospectResponseRetrieved()
    {
        $token = '12312313';
        $header = sprintf('%s: %s', AuthenticationMiddleware::BEARER, $token);
        $this->request->expects($this->once())
            ->method('getHeader')
            ->with(AuthenticationMiddleware::HEADER_KEY)
            ->willReturn([$header]);

        $body = '{
            "active": false,
            "scope": "read",
            "client_id": "J8NFmU4tJVgDxKaJFmXTWvaHO",
            "username": "test",
            "exp":0
        }';
        $invalidIntrospectResponse = $this->createIntrospectResponse($body);
        $this->jwtAdapter->expects($this->once())
            ->method('introspectToken')
            ->with($token)
            ->willReturn($invalidIntrospectResponse);

        $this->authenticationMiddleware->isValid(
            $this->request,
            $this->response,
            function () {
            }
        );
    }

    /**
     * @test
     * @expectedException \RecipeService\SharedContext\Exception\InternalServerException
     */
    public function throwsInternalServerExceptionWhenCallNotValid()
    {
        $token = '12312313';
        $header = sprintf('%s: %s', AuthenticationMiddleware::BEARER, $token);
        $this->request->expects($this->once())
            ->method('getHeader')
            ->with(AuthenticationMiddleware::HEADER_KEY)
            ->willReturn([$header]);

        $this->jwtAdapter->expects($this->once())
            ->method('introspectToken')
            ->with($token)
            ->willThrowException(new \RuntimeException());

        $this->authenticationMiddleware->isValid(
            $this->request,
            $this->response,
            function () {
            }
        );
    }
}

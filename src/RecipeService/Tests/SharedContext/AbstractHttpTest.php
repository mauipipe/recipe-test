<?php

namespace RecipeService\Tests\SharedContext;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 07.05.17
 * Time: 13:27
 */
abstract class AbstractHttpTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ServerRequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $request;
    /**
     * @var StreamInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $stream;
    /**
     * @var ResponseInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $response;

    public function setUp()
    {
        $this->request = $this->getMockBuilder('Psr\Http\Message\ServerRequestInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->response = $this->getMockBuilder('Psr\Http\Message\ResponseInterface')
            ->disableOriginalConstructor()
            ->getMock();


        $this->stream = $this->getMockBuilder('Psr\Http\Message\StreamInterface')
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @param string $body
     */
    protected function prepareMockRequest(string $body): void
    {
        $this->stream->expects($this->once())
            ->method('getContents')
            ->willReturn($body);

        $this->request->expects($this->once())
            ->method('getBody')
            ->willReturn($this->stream);
    }

    /**
     * @param string $body
     */
    protected function prepareMockResponse(string $body): void
    {
        $this->stream->expects($this->once())
            ->method('getContents')
            ->willReturn($body);

        $this->response->expects($this->once())
            ->method('getBody')
            ->willReturn($this->stream);
    }
}

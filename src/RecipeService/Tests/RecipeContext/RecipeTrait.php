<?php

namespace RecipeService\Tests\RecipeContext;

use RecipeService\RecipeContext\Model\Recipe;
use RecipeService\RecipeContext\Repository\RecipeRepository;

/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 03.05.17
 * Time: 20:39
 */
trait RecipeTrait
{
    /**
     * @var int
     */
    protected static $RECIPE_ID = 1;
    /**
     * @var string
     */
    protected static $BODY_NAME = "pizza";
    /**
     * @var string
     */
    protected static $BODY_PREP_TIME = "1232";
    /**
     * @var string
     */
    protected static $DIFFICULTY = 3;
    /**
     * @var bool
     */
    protected static $BODY_IS_VEG = true;

    /**
     * @var float
     */
    protected static $RATING = 2.4;


    /**
     * @param float|null $rating
     *
     * @return Recipe
     */
    protected function getMockRecipe(float $rating = RecipeRepository::INITIAL_RATE_SCORE): Recipe
    {
        return new Recipe(
            self::$RECIPE_ID,
            static::$BODY_NAME,
            self::$BODY_PREP_TIME,
            self::$DIFFICULTY,
            self::$BODY_IS_VEG,
            $rating
        );
    }

    /**
     * @return array
     */
    protected function getMockRequestBody(): array
    {
        return [
            Recipe::NAME       => self::$BODY_NAME,
            Recipe::PREP_TIME  => self::$BODY_PREP_TIME,
            Recipe::DIFFICULTY => self::$DIFFICULTY,
            Recipe::VEGETARIAN => self::$BODY_IS_VEG,
        ];
    }
}

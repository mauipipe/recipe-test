<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 08.05.17
 * Time: 19:06
 */

namespace RecipeService\Tests\RecipeContext\Serializer;

use RecipeService\RecipeContext\Model\Collection\RecipeCollection;
use RecipeService\RecipeContext\Model\Recipe;
use RecipeService\RecipeContext\Model\RequestRecipeArgs;
use RecipeService\RecipeContext\Repository\RecipeRepository;
use RecipeService\RecipeContext\Serializer\RecipeResponseSerializer;
use RecipeService\Tests\RecipeContext\RecipeTrait;

class RecipeResponseSerializerTest extends \PHPUnit_Framework_TestCase
{
    use RecipeTrait;

    /**
     * @test
     * @dataProvider provider
     */
    public function serializeRecipeToResponseArray($rating, $expectedRating)
    {
        $recipe = $this->getMockRecipe($rating);

        $expectedResult = [
            Recipe::ID         => self::$RECIPE_ID,
            Recipe::NAME       => $recipe->getName(),
            Recipe::PREP_TIME  => $recipe->getPrepTime(),
            Recipe::DIFFICULTY => $recipe->getDifficulty(),
            Recipe::VEGETARIAN => $recipe->isVegetarian(),
            Recipe::RATE       => $expectedRating,
        ];

        $result = RecipeResponseSerializer::serialize($recipe);
        $this->assertEquals($expectedResult, $result);
    }

    public function provider()
    {
        return [
            [0, 0.0],
            [2.454545, 2.5],
            [2, 2.0],
            [2.345, 2.3],
        ];
    }

    /**
     * @test
     */
    public function getsAnArrayOfRecipes()
    {
        $recipeCollection = new RecipeCollection();
        $recipe1 = $this->getMockRecipe();
        $recipe2 = $this->getMockRecipe();

        $recipeCollection->addRecipe($recipe1);
        $recipeCollection->addRecipe($recipe2);


        $expectedResult = [
            RecipeResponseSerializer::DATA          => [
                [
                    Recipe::ID         => self::$RECIPE_ID,
                    Recipe::NAME       => self::$BODY_NAME,
                    Recipe::PREP_TIME  => self::$BODY_PREP_TIME,
                    Recipe::DIFFICULTY => self::$DIFFICULTY,
                    Recipe::VEGETARIAN => self::$BODY_IS_VEG,
                    Recipe::RATE       => RecipeRepository::INITIAL_RATE_SCORE,

                ],
                [
                    Recipe::ID         => self::$RECIPE_ID,
                    Recipe::NAME       => self::$BODY_NAME,
                    Recipe::PREP_TIME  => self::$BODY_PREP_TIME,
                    Recipe::DIFFICULTY => self::$DIFFICULTY,
                    Recipe::VEGETARIAN => self::$BODY_IS_VEG,
                    Recipe::RATE       => RecipeRepository::INITIAL_RATE_SCORE,
                ],
            ],
            RecipeResponseSerializer::TOTAL_RECIPES => RecipeCollection::DEFAULT_COUNT,
        ];

        $result = RecipeResponseSerializer::serializeCollection($recipeCollection);

        $this->assertEquals($expectedResult, $result);
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 08.05.17
 * Time: 22:54
 */

namespace RecipeService\Tests\RecipeContext\Model;

use RecipeService\RecipeContext\Model\RequestRecipeArgs;

class RequestRecipeArgsTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function getsQueryParamsCasted()
    {
        $args = [
            'name'       => 'boo',
            'vegetarian' => 'false',
            'difficulty' => '2',
        ];

        $expectedResult = [
            'name'       => 'boo',
            'vegetarian' => 0,
            'difficulty' => 2,
        ];


        $requestRecipeArgs = new RequestRecipeArgs($args);
        $result = $requestRecipeArgs->getQueryParams();

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @test
     * @dataProvider pageProvider
     * @param array $args
     * @param int $expectedResult
     */
    public function getsFirstResult(array $args, int $expectedResult)
    {
        $requestRecipeArgs = new RequestRecipeArgs($args);

        $this->assertEquals($expectedResult, $requestRecipeArgs->getFirstResult());
    }

    /**
     * @return array
     */
    public function pageProvider()
    {
        return [
            [

                [
                    'name'       => 'boo',
                    'vegetarian' => false,
                    'difficulty' => 2,
                    'page'       => 2,
                ],
                1,
            ],
            [
                [
                    'name'       => 'boo',
                    'vegetarian' => false,
                    'difficulty' => 2,
                ],
                0,
            ],
            [
                [
                    'name'       => 'boo',
                    'vegetarian' => false,
                    'difficulty' => 2,
                    'page'       => 1,
                ],
                0,
            ],
        ];
    }

    /**
     * @test
     * @dataProvider offsetProvider
     * @param $args
     * @param $expectedResult
     */
    public function getsOffset($args, $expectedResult)
    {
        $requestRecipeArgs = new RequestRecipeArgs($args);
        ;

        $this->assertEquals($expectedResult, $requestRecipeArgs->getOffset());
    }

    /**
     * @return array
     */
    public function offsetProvider()
    {
        return [
            [

                [
                    'name'                           => 'boo',
                    'vegetarian'                     => false,
                    'difficulty'                     => 2,
                    RequestRecipeArgs::ITEM_PER_PAGE => 2,
                ],
                2,
            ],
            [
                [
                    'name'       => 'boo',
                    'vegetarian' => false,
                    'difficulty' => 2,
                ],
                20,
            ],
            [
                [
                    'name'                           => 'boo',
                    'vegetarian'                     => false,
                    'difficulty'                     => 2,
                    RequestRecipeArgs::ITEM_PER_PAGE => 1,
                ],
                1,
            ],
        ];
    }

    /**
     * @test
     * @dataProvider queryArgsProvider
     * @param $args
     * @param $expectedResult
     */
    public function hasQueryArgsWhenArgsHaveArgsBesidesPaginationValue($args, $expectedResult)
    {
        $requestRecipeArgs = new RequestRecipeArgs($args);
        ;

        $this->assertEquals($expectedResult, $requestRecipeArgs->hasQueryArgs());
    }

    /**
     * @return array
     */
    public function queryArgsProvider()
    {
        return [
            [
                [
                    RequestRecipeArgs::ITEM_PER_PAGE => 2,
                ],
                false,
            ],
            [
                [
                    RequestRecipeArgs::PAGE          => 2,
                    RequestRecipeArgs::ITEM_PER_PAGE => 2,
                ],
                false,
            ],
            [
                [
                    'name'       => 'boo',
                    'vegetarian' => false,
                    'difficulty' => 2,
                ],
                true,
            ],
            [
                [
                ],
                false,
            ],
        ];
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 03.05.17
 * Time: 23:12
 */

namespace RecipeService\Tests\RecipeContext\Model;

use RecipeService\RecipeContext\Model\Collection\RecipeCollection;
use RecipeService\RecipeContext\Model\Recipe;
use RecipeService\RecipeContext\Model\RecipePostBody;
use RecipeService\RecipeContext\Repository\RecipeRepository;
use RecipeService\Tests\RecipeContext\RecipeTrait;

class RecipeCollectionTest extends \PHPUnit_Framework_TestCase
{
    use RecipeTrait;

    /**
     * @test
     */
    public function isEmptyReturnTrueWhenThereIsNoElementInsideCollection()
    {
        $recipeCollection = new RecipeCollection();
        $this->assertEquals(true, $recipeCollection->isEmpty());
    }

    /**
     * @test
     */
    public function isEmptyReturnFalseWhenThereIsAnElementInsideCollection()
    {
        $recipeCollection = new RecipeCollection();
        $recipe1 = $this->getMockRecipe();
        $recipeCollection->addRecipe($recipe1);

        $this->assertEquals(false, $recipeCollection->isEmpty());
    }
}

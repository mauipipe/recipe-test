<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 03.05.17
 * Time: 19:41
 */

namespace RecipeService\Tests\RecipeContext\Service;

use RecipeService\RecipeContext\Model\Collection\RecipeCollection;
use RecipeService\RecipeContext\Model\Recipe;
use RecipeService\RecipeContext\Model\RecipePostBody;
use RecipeService\RecipeContext\Model\RequestRecipeArgs;
use RecipeService\RecipeContext\Repository\RecipeRepositoryInterface;
use RecipeService\RecipeContext\Service\RecipeService;
use RecipeService\SharedContext\Exception\NotFoundException;
use RecipeService\Tests\RecipeContext\RecipeTrait;

class RecipeServiceTest extends \PHPUnit_Framework_TestCase
{
    use RecipeTrait;

    const ID = 1;

    /**
     * @var RecipeRepositoryInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $recipeRepository;
    /**
     * @var RecipeService
     */
    private $recipeService;

    public function setUp()
    {
        $this->recipeRepository = $this->getMockBuilder(
            'RecipeService\RecipeContext\Repository\RecipeRepositoryInterface'
        )->disableOriginalConstructor()->getMock();

        $this->recipeService = new RecipeService($this->recipeRepository);
    }

    /**
     * @test
     */
    public function addsARecipeReturningTheAddedObject()
    {
        $body = $this->getMockRequestBody();

        $recipeBody = new RecipePostBody($body);
        $recipe = $this->getMockRecipe();

        $this->recipeRepository->expects($this->once())
            ->method('addRecipe')
            ->with($recipeBody)
            ->willReturn($recipe);

        $result = $this->recipeService->addRecipe($recipeBody);

        $this->assertEquals($recipe, $result);
    }

    /**
     * @test
     */
    public function getsACollectionOfRecipes()
    {
        $recipeCollection = new RecipeCollection();
        $mockRecipe = $this->getMockRecipe();
        $recipeCollection->addRecipe($mockRecipe);
        $args = [
            'name'       => 'boo',
            'vegetarian' => false,
            'difficulty' => 2,
            'page'       => 2,
        ];
        $recipeQueryArgs = new RequestRecipeArgs($args);

        $this->recipeRepository->expects($this->once())
            ->method('getRecipes')
            ->with($recipeQueryArgs)
            ->willReturn($recipeCollection);

        $result = $this->recipeService->getRecipes($args);

        $this->assertEquals($recipeCollection, $result);
    }

    /**
     * @test
     * @expectedException \RecipeService\SharedContext\Exception\NotFoundException
     */
    public function throwsNotFoundExceptionWhenNoRecipesAreRetrieved()
    {
        $recipeCollection = new RecipeCollection();
        $this->recipeRepository->expects($this->once())
            ->method('getRecipes')
            ->willReturn($recipeCollection);

        $this->recipeService->getRecipes();
    }

    /**
     * @test
     */
    public function getsARecipeById()
    {
        $recipe = $this->getMockRecipe();
        $this->recipeRepository->expects($this->once())
            ->method('getRecipeById')
            ->willReturn($recipe);

        $result = $this->recipeService->getRecipeById(self::ID);

        $this->assertEquals($recipe, $result);
    }

    /**
     * @test
     */
    public function deletesARecipeById()
    {
        $this->recipeRepository->expects($this->once())
            ->method('deleteRecipe')
            ->with(self::ID);

        $this->recipeService->deleteRecipe(self::ID);
    }

    /**
     * @test
     */
    public function updatesARecipeReturningTheAddedObject()
    {
        $body = $this->getMockRequestBody();

        $recipeBody = new RecipePostBody($body);
        $recipe = $this->getMockRecipe();

        $this->recipeRepository->expects($this->once())
            ->method('updateRecipe')
            ->with(self::ID, $recipeBody)
            ->willReturn($recipe);

        $result = $this->recipeService->updateRecipe(self::ID, $recipeBody);

        $this->assertEquals($recipe, $result);
    }

    /**
     * @test
     */
    public function hasRecipeIdReturnTrueWhenNotFoundExceptionIsNotThrown()
    {
        $recipe = $this->getMockRecipe();
        $this->recipeRepository->expects($this->once())
            ->method('getRecipeById')
            ->willReturn($recipe);

        $result = $this->recipeService->hasRecipeId(self::ID);

        $this->assertEquals(true, $result);
    }

    /**
     * @test
     */
    public function hasRecipeIdReturnFalseWhenNotFoundExceptionIsThrown()
    {
        $this->recipeRepository->expects($this->once())
            ->method('getRecipeById')
            ->willThrowException(new NotFoundException());

        $result = $this->recipeService->hasRecipeId(self::ID);

        $this->assertEquals(false, $result);
    }
}

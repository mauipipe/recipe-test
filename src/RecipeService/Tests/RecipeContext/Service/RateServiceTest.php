<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 11:26
 */

namespace RecipeService\Tests\RecipeContext\Service;

use RecipeService\RecipeContext\Model\Rate;
use RecipeService\RecipeContext\Repository\RateRepositoryInterface;
use RecipeService\RecipeContext\Service\RateService;
use RecipeService\RecipeContext\Service\RatesServiceInterface;
use RecipeService\RecipeContext\Service\RecipeServiceInterface;
use RecipeService\Tests\RecipeContext\RecipeTrait;

class RateServiceTest extends \PHPUnit_Framework_TestCase
{
    const MID_RATE = 3;
    use RecipeTrait;

    const RECIPE_ID = 1;
    /**
     * @var RecipeServiceInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $recipeService;
    /**
     * @var RateRepositoryInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $rateRepository;
    /**
     * @var RatesServiceInterface
     */
    private $rateService;

    public function setUp()
    {
        $this->recipeService = $this->getMockBuilder('RecipeService\RecipeContext\Service\RecipeServiceInterface')
            ->disableOriginalConstructor()
            ->getMock();
        $this->rateRepository = $this->getMockBuilder('RecipeService\RecipeContext\Repository\RateRepositoryInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->rateService = new RateService($this->recipeService, $this->rateRepository);
    }

    /**
     * @test
     */
    public function updatesRateScore()
    {
        $rate = new Rate(self::RECIPE_ID, self::MID_RATE);

        $this->recipeService->expects($this->once())
            ->method('hasRecipeId')
            ->with(self::RECIPE_ID)
            ->willReturn(true);

        $this->rateRepository->expects($this->once())
            ->method('updateRecipeRate')
            ->with($rate);

        $this->rateService->rateRecipe($rate);
    }
}

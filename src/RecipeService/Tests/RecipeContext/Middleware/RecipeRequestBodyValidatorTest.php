<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 05.05.17
 * Time: 19:34
 */

namespace RecipeService\Tests\RecipeContext\Middleware;

use RecipeService\RecipeContext\Middleware\RecipeRequestBodyValidator;
use RecipeService\Tests\SharedContext\AbstractHttpTest;
use Zend\Diactoros\Response;

class RecipeRequestBodyValidatorTest extends AbstractHttpTest
{
    const MALFORMED_JSON = '{';
    /**
     * @var RecipeRequestBodyValidator
     */
    private $validator;

    public function setUp()
    {
        parent::setUp();

        $this->validator = new RecipeRequestBodyValidator();
    }

    /**
     * @test
     * @expectedException \RecipeService\RecipeContext\Exception\InvalidRecipeDifficultyException
     */
    public function throwInvalidRecipeDifficultyExceptionWhenDifficultyGreaterThan5()
    {
        $body = '{
            "name": "pizza",
            "prep_time": "1232",
            "difficulty":9,
            "vegetarian": "true"
        }';
        $this->prepareMockRequest($body);

        $response = new Response();
        $next = function ($request, $response) {
            return $response;
        };

        $this->validator->isValid($this->request, $response, $next);
    }

    /**
     * @test
     * @expectedException \RecipeService\SharedContext\Exception\BadRequestException
     * @dataProvider badRequestProvider
     * @param string $body
     */
    public function throwBadRequestExceptionWhenMalformedJsonBodySent(string $body)
    {
        $this->prepareMockRequest($body);

        $response = new Response();
        $next = function ($request, $response) {
            return $response;
        };

        $this->validator->isValid($this->request, $response, $next);
    }

    /**
     * @return array
     */
    public function badRequestProvider()
    {
        return [
            [self::MALFORMED_JSON],
            ['{"fasdsad":"dsd"}'],
        ];
    }

    /**
     * @test
     */
    public function returnResponseWhenAValidRequestPostBodyIsSent()
    {
        $postBody = '{"name": "pizza","prep_time": "1232","difficulty":3,"vegetarian":"true"}';
        $response = new Response();
        $next = function ($request, $response) {
            return $response;
        };

        $this->prepareMockRequest($postBody);

        $expectedResult = $this->createPostBody($response, $postBody);
        $result = $this->validator->isValid($this->request, $response, $next);
        $this->assertEquals($expectedResult, $result);
    }


    /**
     * @param Response $response
     * @param string $msg
     * @return Response
     */
    private function createPostBody(Response $response, string $msg): Response
    {
        $expectedResult = clone $response;
        $expectedResult->getBody()->write($msg);

        return $expectedResult;
    }
}

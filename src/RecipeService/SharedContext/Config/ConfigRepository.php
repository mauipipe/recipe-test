<?php

namespace RecipeService\SharedContext\Config;

use RecipeService\SharedContext\Exception\UndefinedConfigValueException;

class ConfigRepository implements ConfigRepositoryInterface
{
    /**
     * @var array
     */
    private $configData;

    /**
     * @param string $configFilePath
     */
    public function __construct(string $configFilePath)
    {
        $fileContent = file_get_contents($configFilePath);
        $this->configData = json_decode($fileContent, true);
    }

    /**
     * @inheritdoc
     *
     * @return mixed
     */
    public function getValue(string $key): array
    {
        if (!isset($this->configData[$key])) {
            throw new UndefinedConfigValueException($key);
        }

        return $this->configData[$key];
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 02.05.17
 * Time: 22:10
 */

namespace RecipeService\SharedContext\Config;

use RecipeService\SharedContext\Exception\UndefinedConfigValueException;

interface ConfigRepositoryInterface
{
    /**
     * @param string $key
     * @return array
     *
     * @throws UndefinedConfigValueException
     */
    public function getValue(string $key): array;
}

<?php

namespace RecipeService\SharedContext\DI;

use Pimple\Container as PimpleContainer;

class ContainerFactory
{
    /**
     * @param array $dependencies
     * @return PimpleContainer
     * @internal param string $configPath
     */
    public static function createContainer(array $dependencies): PimpleContainer
    {
        return new PimpleContainer($dependencies);
    }
}

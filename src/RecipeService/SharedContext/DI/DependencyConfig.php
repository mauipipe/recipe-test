<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 02.05.17
 * Time: 22:03
 */

namespace RecipeService\SharedContext\DI;

use GuzzleHttp\Client;
use League\Container\Container;
use League\Route\RouteCollection;
use RecipeService\RecipeContext\Controller\RatesController;
use RecipeService\RecipeContext\Controller\RecipeController;
use RecipeService\RecipeContext\Middleware\RecipeRequestBodyValidator;
use RecipeService\RecipeContext\Middleware\RecipeResponsePagination;
use RecipeService\RecipeContext\Repository\RateRepository;
use RecipeService\RecipeContext\Repository\RecipeRepository;
use RecipeService\RecipeContext\Service\RateService;
use RecipeService\RecipeContext\Service\RecipeService;
use RecipeService\SharedContext\Auth\JwtAuthAdapter;
use RecipeService\SharedContext\Config\ConfigRepository;
use RecipeService\SharedContext\Config\ConfigRepositoryInterface;
use RecipeService\SharedContext\Db\DbBalManager;
use RecipeService\SharedContext\Middleware\AuthenticationMiddleware;
use RecipeService\SharedContext\Router\Router;

class DependencyConfig
{
    /**
     * @var string
     */
    public static $configFilePath = __DIR__."/../../../../config/config.json";

    /**
     * @param string $configFilePath
     */
    public static function setConfigFilePath(string $configFilePath)
    {
        static::$configFilePath = $configFilePath;
    }

    /**
     * @return array
     */
    public static function getDependencies(): array
    {
        return [
            'db_manager'                      => function ($c) {
                /** @var ConfigRepository $config */
                $config = $c['config'];

                return new DbBalManager($config->getValue("dbal"));
            },
            'config'                          => function ($c) {
                return new ConfigRepository(static::$configFilePath);
            },
            'router'                          => function ($c) {
                $routeContainer = new Container();
                $routeCollection = new RouteCollection($routeContainer);

                return new Router(new Container(), $c, $routeCollection);
            },
            RecipeRepository::class           => function ($c) {
                /** @var DbBalManager $dbManager */
                $dbManager = $c['db_manager'];

                return new RecipeRepository($dbManager->getQueryBuilder(), $dbManager->getConnection());
            },
            RecipeService::class              => function ($c) {
                return new RecipeService($c[RecipeRepository::class]);
            },
            AuthenticationMiddleware::class   => function ($c) {
                /** @var ConfigRepositoryInterface $config */
                $config = $c['config'];
                $authServiceConfig = $config->getValue('auth_service');

                return new AuthenticationMiddleware($c[JwtAuthAdapter::class], $authServiceConfig['scopes']);
            },
            RecipeRequestBodyValidator::class => function ($c) {
                return new RecipeRequestBodyValidator();
            },
            RecipeResponsePagination::class   => function ($c) {
                return new RecipeResponsePagination();
            },
            RecipeController::class           => function ($c) {
                return new RecipeController($c[RecipeService::class]);
            },
            JwtAuthAdapter::class             => function ($c) {
                /** @var ConfigRepositoryInterface $config */
                $config = $c['config'];
                $authServiceConfig = $config->getValue('auth_service');
                $client = new Client([
                    'base_uri' => $authServiceConfig['host'],
                ]);

                return new JwtAuthAdapter($client);
            },
            RatesController::class            => function ($c) {
                return new RatesController($c[RateService::class]);
            },
            RateService::class                => function ($c) {
                return new RateService($c[RecipeService::class], $c[RateRepository::class]);
            },
            RateRepository::class             => function ($c) {
                /** @var DbBalManager $dbManager */
                $dbManager = $c['db_manager'];

                return new RateRepository($dbManager->getQueryBuilder(), $dbManager->getConnection());
            },
        ];
    }
}

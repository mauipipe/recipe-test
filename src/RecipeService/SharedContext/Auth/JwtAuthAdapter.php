<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 18:18
 */

namespace RecipeService\SharedContext\Auth;

use GuzzleHttp\Client;
use RecipeService\SharedContext\Enum\InternalEndpoints;
use RecipeService\SharedContext\Exception\NotAuthorizedException;
use RecipeService\SharedContext\Model\IntrospectResponse;

class JwtAuthAdapter
{
    const APPLICATION_JSON = 'application-json';
    /**
     * @var Client
     */
    private $client;
    /**
     * @var array
     */
    private $scopes;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function introspectToken(string $token): IntrospectResponse
    {
        $response = $this->client->post(
            InternalEndpoints::TOKEN_INTROSPECT,
            [
                'headers' => ['content-type' => self::APPLICATION_JSON],
                'body'    => json_encode(['token' => $token]),
            ]
        );

        $body = json_decode($response->getBody()->getContents(), true);

        if (null === $body) {
            throw new NotAuthorizedException();
        }

        $this->validateIntrospectResponse($body);

        return new IntrospectResponse(
            $body[IntrospectResponse::ACTIVE],
            $body[IntrospectResponse::SCOPE],
            $body[IntrospectResponse::CLIENT_ID],
            $body[IntrospectResponse::USERNAME],
            $body[IntrospectResponse::EXPIRATION]
        );
    }

    /**
     * @param $body
     * @throws NotAuthorizedException
     */
    private function validateIntrospectResponse($body)
    {
        $body = array_keys($body);

        $mandatoryKeys = IntrospectResponse::$mandatoryKeys;
        $includedValues = array_intersect($body, $mandatoryKeys);
        if (count($includedValues) < count($mandatoryKeys)) {
            throw new NotAuthorizedException();
        }
    }
}

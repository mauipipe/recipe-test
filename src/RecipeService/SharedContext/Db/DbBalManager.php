<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 01.05.17
 * Time: 21:02
 */

namespace RecipeService\SharedContext\Db;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Query\QueryBuilder;

class DbBalManager
{
    /**
     * @var array
     */
    private $dbOptions;
    /**
     * @var Connection|null
     */
    private $connection;

    /**
     * @param array $dbOptions
     */
    public function __construct(array $dbOptions)
    {
        $this->dbOptions = $dbOptions;
    }

    /**
     * @return Connection
     */
    public function getConnection(): Connection
    {
        $this->initConnection();

        return $this->connection;
    }

    /**
     * @return QueryBuilder
     */
    public function getQueryBuilder(): QueryBuilder
    {
        $this->initConnection();

        return $this->connection->createQueryBuilder();
    }

    private function initConnection()
    {
        if (null === $this->connection) {
            $this->connection = DriverManager::getConnection($this->dbOptions);
        }
    }
}

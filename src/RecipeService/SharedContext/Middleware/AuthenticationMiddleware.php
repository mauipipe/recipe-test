<?php

namespace RecipeService\SharedContext\Middleware;

use League\Route\Http\Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use RecipeService\SharedContext\Auth\JwtAuthAdapter;
use RecipeService\SharedContext\Exception\InternalServerException;
use RecipeService\SharedContext\Exception\NotAuthorizedException;

/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 19:28
 */
class AuthenticationMiddleware implements MiddlewareInterface
{
    const BEARER = 'Bearer';
    const BEARER_DELIMETER = ': ';
    const HEADER_KEY = 'Authorization';
    const BEARER_PARAMS = 2;
    /**
     * @var JwtAuthAdapter
     */
    private $jwtAuthAdapter;
    /**
     * @var array
     */
    private $scopes;

    /**
     * @param JwtAuthAdapter $jwtAuthAdapter
     * @param string $scopes
     */
    public function __construct(JwtAuthAdapter $jwtAuthAdapter, string $scopes)
    {
        $this->jwtAuthAdapter = $jwtAuthAdapter;
        $this->scopes = $scopes;
    }

    /**
     * @inheritdoc
     */
    public function isValid(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ): ResponseInterface {
        $header = $request->getHeader(self::HEADER_KEY);
        if (null === $header || empty($header)) {
            throw new NotAuthorizedException();
        }
        $headerData = explode(self::BEARER_DELIMETER, $header[0]);
        if (count($headerData) !== self::BEARER_PARAMS) {
            throw new NotAuthorizedException();
        }

        $bearer = $headerData[0];
        $token = $headerData[1];
        if (trim($bearer) !== self::BEARER) {
            throw new NotAuthorizedException();
        }

        try {
            $introspectResponse = $this->jwtAuthAdapter->introspectToken($token);
            $introspectResponse->validate($this->scopes, time());
        } catch (NotAuthorizedException $e) {
            throw $e;
        } catch (\RuntimeException $e) {
            throw new InternalServerException($e);
        }

        $response = $next($request, $response);

        return $response;
    }
}

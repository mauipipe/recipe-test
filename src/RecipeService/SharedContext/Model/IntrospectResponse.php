<?php

namespace RecipeService\SharedContext\Model;

use RecipeService\SharedContext\Exception\NotAuthorizedException;

/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 07.05.17
 * Time: 13:02
 */
class IntrospectResponse
{
    const ACTIVE = 'active';
    const SCOPE = 'scope';
    const CLIENT_ID = 'client_id';
    const USERNAME = 'username';
    const EXPIRATION = 'exp';
    const SCOPE_DELIMITER = ',';
    const INFINITE = 0;

    /**
     * @var bool
     */
    private $isActive;
    /**
     * @var string
     */
    private $scope;
    /**
     * @var string
     */
    private $clientId;
    /**
     * @var string
     */
    private $username;
    /**
     * @var int
     */
    private $exp;

    public static $mandatoryKeys = [
        IntrospectResponse::ACTIVE,
        IntrospectResponse::SCOPE,
        IntrospectResponse::CLIENT_ID,
        IntrospectResponse::USERNAME,
        IntrospectResponse::EXPIRATION,

    ];

    /**
     * IntrospectResponse constructor.
     * @param bool $isActive
     * @param string $scope
     * @param string $clientId
     * @param string $username
     * @param int $exp
     */
    public function __construct(
        bool $isActive,
        string $scope,
        string $clientId,
        string $username,
        int $exp
    ) {
        $this->isActive = $isActive;
        $this->scope = $scope;
        $this->clientId = $clientId;
        $this->username = $username;
        $this->exp = $exp;
    }

    /**
     * @param string $scope
     * @param int $currentTimestamp
     * @throws NotAuthorizedException
     */
    public function validate(string $scope, int $currentTimestamp)
    {
        if (!$this->isActive) {
            throw new NotAuthorizedException();
        }

        if (false === strpos($scope, $this->scope)) {
            throw new NotAuthorizedException();
        }

        if ($this->exp !== self::INFINITE && $this->exp < $currentTimestamp) {
            throw new NotAuthorizedException();
        }
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 02.05.17
 * Time: 23:03
 */

namespace RecipeService\SharedContext\Router;

use RecipeService\RecipeContext\Controller\RatesController;
use RecipeService\RecipeContext\Controller\RecipeController;
use RecipeService\RecipeContext\Middleware\RecipeRequestBodyValidator;
use RecipeService\RecipeContext\Middleware\RecipeResponsePagination;
use RecipeService\SharedContext\Enum\Methods;
use RecipeService\SharedContext\Middleware\AuthenticationMiddleware;

class RouteConfig
{
    /**
     * @return array
     */
    public static function getRoutes(): array
    {
        return [
            [
                'method'  => Methods::PUT,
                'url'     => '/recipes/{id:number}/rates',
                'handler' => RatesController::class,
                'action'  => 'rateRecipe',
            ],
            [
                'method'     => Methods::POST,
                'url'        => '/recipes',
                'handler'    => RecipeController::class,
                'action'     => 'addRecipe',
                'middleware' => [
                    [RecipeRequestBodyValidator::class, 'isValid'],
                    [AuthenticationMiddleware::class, 'isValid'],
                ],
            ],
            [
                'method'  => Methods::GET,
                'url'     => '/recipes',
                'handler' => RecipeController::class,
                'action'  => 'getRecipes',
                'middleware' => [
                    [RecipeResponsePagination::class, 'isValid'],
                ],
            ],
            [
                'method'  => Methods::GET,
                'url'     => '/recipes/{id:number}',
                'handler' => RecipeController::class,
                'action'  => 'getRecipeById',
            ],
            [
                'method'  => Methods::PATCH,
                'url'     => '/recipes/{id:number}',
                'handler' => RecipeController::class,
                'action'  => 'updateRecipe',
                'middleware' => [
                    [AuthenticationMiddleware::class, 'isValid'],
                ],
            ],
            [
                'method'  => Methods::DELETE,
                'url'     => '/recipes/{id:number}',
                'handler' => RecipeController::class,
                'action'  => 'deleteRecipe',
                'middleware' => [
                    [AuthenticationMiddleware::class, 'isValid'],
                ],
            ],
        ];
    }
}

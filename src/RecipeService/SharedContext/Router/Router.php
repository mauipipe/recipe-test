<?php

namespace RecipeService\SharedContext\Router;

use League\Container\Container as RouteContainer;
use League\Route\Route;
use League\Route\RouteCollection;
use League\Route\Strategy\StrategyInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Pimple\Container;
use RecipeService\SharedContext\Exception\HandlerNotFoundException;
use RecipeService\SharedContext\Exception\UndefinedConfigValueException;
use RecipeService\SharedContext\Middleware\MiddlewareInterface;
use Zend\Diactoros\Response;
use Zend\Diactoros\ServerRequestFactory;

class Router
{
    const REQUEST = 'request';
    const RESPONSE = 'response';
    const EMITTER = 'emitter';
    const MIDDLEWARE = 'middleware';

    /**
     * @var RouteContainer
     */
    private $routeContainer;
    /**
     * @var Container
     */
    private $container;
    /**
     * @var RouteCollection
     */
    private $routeCollection;

    /**
     * @param RouteContainer $routeContainer
     * @param Container $container
     * @param RouteCollection $routeCollection
     */
    public function __construct(
        RouteContainer $routeContainer,
        Container $container,
        RouteCollection $routeCollection
    ) {
        $this->routeContainer = $routeContainer;
        $this->sharedResponse = $this->routeContainer->share(self::RESPONSE, Response::class);
        $this->sharedRequest = $this->routeContainer->share(self::REQUEST, function () {
            return ServerRequestFactory::fromGlobals(
                $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
            );
        });
        $this->container = $container;
        $this->routeCollection = $routeCollection;
    }

    /**
     * @param StrategyInterface $strategy
     * @param array $routes
     */
    public function initRoutes(StrategyInterface $strategy, array $routes)
    {
        $this->routeContainer->share(self::EMITTER, Response\SapiEmitter::class);
        $this->routeCollection = $this->createRouteCollection($strategy, $routes);
        ;
    }

    public function dispatch()
    {
        $response = $this->routeCollection->dispatch(
            $this->routeContainer->get(self::REQUEST),
            $this->routeContainer->get(self::RESPONSE)
        );

        $this->routeContainer->get(self::EMITTER)->emit($response);
    }

    /**
     * @return RouteCollection
     */
    public function getRouteCollection(): RouteCollection
    {
        return $this->routeCollection;
    }

    /**
     * @param StrategyInterface $strategy
     * @param array $routes
     * @return RouteCollection
     * @throws HandlerNotFoundException
     * @throws UndefinedConfigValueException
     */
    private function createRouteCollection(StrategyInterface $strategy, array $routes): RouteCollection
    {
        $this->routeCollection->setStrategy($strategy);

        foreach ($routes as $route) {
            if (!isset($route['handler'])) {
                throw new UndefinedConfigValueException('missing handler');
            }
            $handler = $this->container->offsetGet($route['handler']);

            if (null === $handler || !$handler instanceof RouterInterface) {
                throw new HandlerNotFoundException(sprintf("cannot find handler %s", get_class($handler)));
            }

            $middlewareContainer = $this->createMiddlewareContainer($route);

            $action = $route['action'];
            $route = $this->routeCollection->map(
                $route['method'],
                $route['url'],
                function (ServerRequestInterface $request, ResponseInterface $response, array $args) use (
                    $handler,
                    $action
                ) {
                    return call_user_func_array([$handler, $action], [$request, $response, $args]);
                }
            );
            foreach ($middlewareContainer as $middleware) {
                $route->middleware($middleware);
            }
        }

        return $this->routeCollection;
    }

    /**
     * @param $route
     * @return array
     */
    private function createMiddlewareContainer(array $route): array
    {
        $middlewareData = isset($route[self::MIDDLEWARE]) ? $route[self::MIDDLEWARE] : [];

        $middlewareContainer = [];
        foreach ($middlewareData as $middleware) {
            $middlewareName = $middleware[0];
            $middlewareObj = $this->container->offsetGet($middlewareName);
            $middleware = [$middlewareObj, $middleware[1]];
            $middlewareContainer[] = $middleware;
        }


        return $middlewareContainer;
    }
}

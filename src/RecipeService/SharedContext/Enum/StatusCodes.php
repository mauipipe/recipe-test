<?php

namespace RecipeService\SharedContext\Enum;

/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 02.05.17
 * Time: 23:08
 */
final class StatusCodes
{
    const STATUS_CREATED = 201;
    const STATUS_NO_CONTENT = 204;
    const BAD_REQUEST = 400;
    const NOT_FOUND = 404;
    const NOT_AUTHORIZED = 401;
    const INTERNAL_SERVER_ERROR = 500;
    const STATUS_OK = 200;
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 19:13
 */

namespace RecipeService\SharedContext\Enum;

final class InternalEndpoints
{
    const TOKEN_INTROSPECT = 'token/introspect';
}

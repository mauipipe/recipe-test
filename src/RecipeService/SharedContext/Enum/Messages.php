<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 05.05.17
 * Time: 20:15
 */

namespace RecipeService\SharedContext\Enum;

class Messages
{
    const MALFORMED_BODY = 'malformed body';
    const NOT_FOUND = 'not found';
    const NOT_AUTHORIZED = 'not authorized';
}

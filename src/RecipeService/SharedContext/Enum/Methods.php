<?php

namespace RecipeService\SharedContext\Enum;

/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 02.05.17
 * Time: 23:10
 */
final class Methods
{
    const POST = 'POST';
    const PUT = 'PUT';
    const GET = 'GET';
    const PATCH = 'PATCH';
    const DELETE = 'DELETE';
}

<?php

namespace RecipeService\SharedContext\Kernel;

use League\Route\Strategy\StrategyInterface;
use RecipeService\SharedContext\DI\ContainerFactory;
use RecipeService\SharedContext\Router\Router;

/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 01.05.17
 * Time: 20:57
 */
class Kernel
{
    /**
     * @param array $dependencies
     * @param array $routes
     * @param StrategyInterface $strategy
     * @return Router
     */
    public static function init(array $dependencies, array $routes, StrategyInterface $strategy): Router
    {
        $container = ContainerFactory::createContainer($dependencies);
        /** @var Router $router */
        $router = $container->offsetGet('router');
        $router->initRoutes($strategy, $routes);

        return $router;
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 19:36
 */

namespace RecipeService\SharedContext\Exception;

use League\Route\Http\Exception;
use RecipeService\SharedContext\Enum\StatusCodes;

class InternalServerException extends Exception
{

    /**
     * InternalServerException constructor.
     * @param \Exception|null $previous
     */
    public function __construct(\Exception $previous = null)
    {
        parent::__construct(
            StatusCodes::INTERNAL_SERVER_ERROR,
            $previous->getMessage(),
            $previous,
            ['Content-Type' => 'application-json'],
            StatusCodes::INTERNAL_SERVER_ERROR
        );
    }
}

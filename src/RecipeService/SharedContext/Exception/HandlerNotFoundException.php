<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 02.05.17
 * Time: 22:56
 */

namespace RecipeService\SharedContext\Exception;

class HandlerNotFoundException extends \Exception
{

    /**
     * HandlerNotFoundException constructor.
     * @param $msg
     */
    public function __construct($msg)
    {
        parent::__construct($msg);
    }
}

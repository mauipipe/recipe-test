<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 02.05.17
 * Time: 20:42
 */

namespace RecipeService\SharedContext\Exception;

class UndefinedConfigValueException extends \Exception
{

    /**
     * @param string $msg
     */
    public function __construct($msg)
    {
        parent::__construct($msg);
    }
}

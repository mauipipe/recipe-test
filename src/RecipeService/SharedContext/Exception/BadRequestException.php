<?php

namespace RecipeService\SharedContext\Exception;

use League\Route\Http\Exception;
use RecipeService\SharedContext\Enum\Messages;
use RecipeService\SharedContext\Enum\StatusCodes;

/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 05.05.17
 * Time: 20:15
 */
class BadRequestException extends Exception
{
    public function __construct(\Exception $previous = null)
    {
        parent::__construct(
            StatusCodes::BAD_REQUEST,
            Messages::MALFORMED_BODY,
            $previous,
            ['Content-Type' => 'application-json'],
            StatusCodes::BAD_REQUEST
        );
    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: david.contavalli
 * Date: 06.05.17
 * Time: 18:48
 */

namespace RecipeService\SharedContext\Exception;

use League\Route\Http\Exception;
use RecipeService\SharedContext\Enum\Messages;
use RecipeService\SharedContext\Enum\StatusCodes;

class NotAuthorizedException extends Exception
{

    /**
     * NotAuthorizedException constructor.
     * @param \Exception|null $previous
     */
    public function __construct(\Exception $previous = null)
    {
        parent::__construct(
            StatusCodes::NOT_AUTHORIZED,
            Messages::NOT_AUTHORIZED,
            $previous,
            ['Content-Type' => 'application-json'],
            StatusCodes::NOT_AUTHORIZED
        );
    }
}

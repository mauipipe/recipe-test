Feature:
  In order to save,list and delete recipes
  As an API user
  I should be able to perform CRUD operation through GET,PATCH,DELETE,POST method on /recipes endpoint

  Background:
    Given my system is empty

  @recipes @status400 @malformed_body
  Scenario Outline: Adds a recipe through
    Given I set header "Authorization" with value "Bearer: 424134134"
    When I send a <verb> request to "/recipes" with body:
    """
    <body>
    """
    Then the response code should be 400
    And the response should contain json:
    """
    {
      "status_code": 400,
      "reason_phrase": "malformed body"
    }
    """
    Examples:
      | body              | verb |
      | null              | POST |
      | {"fasds":"sadsd"} | POST |

  @recipes @status400 @invalid_difficulty
  Scenario: Adds a recipe through
    Given I set header "Authorization" with value "Bearer: 424134134"
    When I send a POST request to "/recipes" with body:
    """
    {
        "name": "pizza",
        "prep_time": "1232",
        "difficulty":9,
        "vegetarian": "true"
    }
    """
    Then the response code should be 400
    And the response should contain json:
    """
    {
        "status_code": 400,
        "reason_phrase": "invalid limit sent 9, max 5"
    }
    """

  @recipes @status404
  Scenario Outline: returns a 404 when no resource is found
    When I send a GET request to "<url>"
    Then the response code should be 404
    And the response should contain json:
    """
    {
      "status_code": 404,
      "reason_phrase": "not found"
    }
    """
    Examples:
      | url        |
      | /recipes   |
      | /recipes/1 |
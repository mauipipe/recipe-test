<?php

use Behat\Gherkin\Node\PyStringNode;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Context\Context;
use RecipeService\SharedContext\Db\DbBalManager;
use RecipeService\SharedContext\DI\ContainerFactory;
use RecipeService\SharedContext\DI\DependencyConfig;
use RecipeService\RecipeContext\Repository\RecipeRepository;
use RecipeService\RecipeContext\Repository\RecipeRepositoryInterface;
use RecipeService\RecipeContext\Repository\RateRepository;
use RecipeService\RecipeContext\Model\Recipe;
use RecipeService\RecipeContext\Model\RecipePostBody;
use RecipeService\RecipeContext\Model\Rate;

require __DIR__.'/../../vendor/autoload.php';

/**
 * Defines application features from the specific context.
 */
class DbContext implements Context
{
    /**
     * @var RecipeRepositoryInterface
     */
    private $recipeRepository;
    /**
     * @var DbBalManager
     */
    private $dbManager;
    /**
     * @var ContainerFactory
     */
    private $container;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $config = __DIR__.'/../../config/config_test.json';
        DependencyConfig::setConfigFilePath($config);
        $this->container = ContainerFactory::createContainer(DependencyConfig::getDependencies());
        /** @var DbBalManager dbManager */
        $this->dbManager = $this->container->offsetGet("db_manager");
        $this->recipeRepository = $this->container->offsetGet(RecipeRepository::class);
    }


    /**
     * @Given my system is empty
     */
    public function mySystemIsEmpty()
    {
        $dbSchema = $this->createDbSchema();
        $conn = $this->dbManager->getConnection();
        $platform = $conn->getDatabasePlatform();

        $sm = $conn->getSchemaManager();
        if ($sm->tablesExist([RateRepository::RATES_TABLE, RecipeRepository::RECIPES_TABLE])) {
            $dropSql = $dbSchema->toDropSql($platform);
            $dropSql = implode(';', $dropSql);
            $conn->exec($dropSql);
        }

        $sql = $dbSchema->toSql($platform);
        $sql = implode(';', $sql);
        $conn->exec($sql);
        $conn->close();
    }

    /**
     * @Then I will have :arg3 :arg1 in my system with :arg2 :arg4
     *
     * @param string $expectedResult
     * @param string $table
     * @param string $idCol
     * @param string $id
     */
    public function iWillHaveInMySystemWith(
        string $expectedResult,
        string $table,
        string $idCol,
        string $id
    ) {
        $result = $this->dbManager->getQueryBuilder()->select()
            ->from($table)
            ->where(sprintf('%s=?', $idCol))
            ->setParameter(0, $id)
            ->execute();

        \PHPUnit\Framework\Assert::assertEquals((int)$expectedResult, $result->rowCount());
    }

    /**
     * @Given my system has :arg1:
     * @param $arg1
     * @param TableNode $table
     */
    public function mySystemHas(
        $arg1,
        TableNode $table
    ) {
        $recipes = $table->getHash();
        foreach ($recipes as $recipe) {
            $recipe = new RecipePostBody(
                $recipe
            );

            $this->recipeRepository->addRecipe($recipe);
        }
    }

    /**
     * @Given they have rates:
     * @param TableNode $ratesTable
     */
    public function theyHaveRates(TableNode $ratesTable)
    {
        $ratesData = $ratesTable->getHash();
        /** @var RateRepository $rateRepository */
        $rateRepository = $this->container->offsetGet(RateRepository::class);
        foreach ($ratesData as $rate) {
            $recipeRates = explode(',', $rate['rates']);
            foreach ($recipeRates as $recipeRate) {
                $rateObj = new Rate($rate['recipe_id'], $recipeRate);
                $rateRepository->updateRecipeRate($rateObj);
            }
        }
    }

    /**
     * @Then the update recipe looks like:
     *
     * @param PyStringNode $string
     */
    public function theUpdateRecipeLooksLike(PyStringNode $string)
    {
        $recipeData = json_decode($string, true);
        $query = $this->dbManager->getQueryBuilder()->select(
            Recipe::ID,
            Recipe::NAME,
            Recipe::PREP_TIME,
            Recipe::DIFFICULTY,
            Recipe::VEGETARIAN
        )
            ->from(RecipeRepository::RECIPES_TABLE)
            ->where('id=?')
            ->setParameter(0, $recipeData['id']);

        $result = $query->execute()->fetch();
        \PHPUnit\Framework\Assert::assertEquals($recipeData, $result);
    }

    /**
     * @return \Doctrine\DBAL\Schema\Schema
     */
    private function createDbSchema(): \Doctrine\DBAL\Schema\Schema
    {
        $schema = new \Doctrine\DBAL\Schema\Schema();
        $recipeTable = $schema->createTable(RecipeRepository::RECIPES_TABLE);
        $recipeTable->addColumn(Recipe::ID, "integer", ["unsigned" => true, "autoincrement" => true]);
        $recipeTable->addColumn(Recipe::NAME, "string", ["length" => 255]);
        $recipeTable->addColumn(Recipe::PREP_TIME, "integer", ["unsigned" => true]);
        $recipeTable->addColumn(Recipe::DIFFICULTY, "integer", ["unsigned" => true]);
        $recipeTable->addColumn(Recipe::VEGETARIAN, "boolean");
        $recipeTable->setPrimaryKey(["id"]);

        $rateTable = $schema->createTable(RateRepository::RATES_TABLE);
        $rateTable->addColumn(Recipe::ID, "integer", ["unsigned" => true, "autoincrement" => true]);
        $rateTable->addColumn('recipe_id', "integer", ["unsigned" => true]);
        $rateTable->addColumn('rate', "decimal", ["unsigned" => true]);

        $rateTable->setPrimaryKey(["id"]);
        $rateTable->addForeignKeyConstraint($recipeTable, ["recipe_id"], [Recipe::ID], ["onDelete" => "CASCADE"]);

        return $schema;
    }
}

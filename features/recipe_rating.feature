Feature:
  In order to save,list and delete recipes
  As an API user
  I should be able to perform CRUD operation through GET,PATCH,DELETE,POST method on /recipes endpoint

  Background:
    Given my system is empty

  @recipes @rating
  Scenario: rates from 1 to 5 a recipe
    Given my system has "recipe":
      | id | name  | prep_time | difficulty | vegetarian |
      | 1  | pizza | 1232      | 3          | true       |
    When I send a PUT request to "/recipes/1/rates" with body:
    """
    {
        "rate": "2"
    }
    """
    Then the response code should be 204

  @recipes @rating @status404
  Scenario: return 404 an recipe with invalid Id is rated
    Given my system has "recipe":
      | id | name  | prep_time | difficulty | vegetarian |
      | 1  | pizza | 1232      | 3          | true       |
    When I send a PUT request to "/recipes/2/rates" with body:
    """
    {
        "rate": "2"
    }
    """
    Then the response code should be 404

Feature:
  In order to save,list and delete recipes
  As an API user
  I should be able to perform CRUD operation through GET,PATCH,DELETE,POST method on /recipes endpoint

  Background:
    Given my system is empty

  @recipes @add
  Scenario: Adds a recipe through
    Given I set header "Authorization" with value "Bearer: 424134134"
    When I send a POST request to "/recipes" with body:
    """
    {
        "name": "pizza",
        "prep_time": "1232",
        "difficulty":3,
        "vegetarian": "true"
    }
    """
    Then the response code should be 201
    And the response should contain json:
    """
    {
      "data": {
          "id": 0,
          "name": "pizza",
          "prep_time": 1232,
          "difficulty":3,
          "vegetarian": true,
          "rate":0
      }
    }
    """
    And I will have 1 "recipes" in my system with "id" 1


  @recipes @list
  Scenario: Retrieves a list of recipes
    Given my system has "recipe":
      | id | name         | prep_time | difficulty | vegetarian |
      | 1  | pizza        | 1232      | 3          | true       |
      | 2  | pizza salami | 1234      | 2          | false      |
    And they have rates:
      | recipe_id | rates   |
      | 1         | 2,3,4,6 |
      | 2         | 1,2,4,6 |
    When I send a GET request to "/recipes"
    Then the response code should be 200
    And the response should contain json:
    """
    {
      "data": [{
          "id": 1,
          "name": "pizza",
          "prep_time": 1232,
          "difficulty": 3,
          "vegetarian": true,
          "rate": 3.8
      }, {
          "id": 2,
          "name": "pizza salami",
          "prep_time": 1234,
          "difficulty": 2,
          "vegetarian": false,
          "rate": 3.3
      }],
      "total_recipes": 2
    }
    """

  @recipes @list @pagination
  Scenario: Retrieves a list of recipes with 1 recipe per page
    Given my system has "recipe":
      | id | name            | prep_time | difficulty | vegetarian |
      | 1  | pizza           | 1232      | 3          | true       |
      | 2  | pizza salami    | 1234      | 2          | false      |
      | 3  | pizza mushrooms | 1234      | 4          | true       |
    And they have rates:
      | recipe_id | rates   |
      | 1         | 2,3,4,6 |
      | 2         | 1,2,4,6 |
      | 3         | 1,2,4,6 |
    When I send a GET request to "/recipes?page=2&item_per_page=1"
    And print response
    Then the response code should be 200
    And the response should contain json:
    """
    {
      "data": [
        {
            "id": 2,
            "name": "pizza salami",
            "prep_time": 1234,
            "difficulty":2,
            "vegetarian": false,
            "rate":3.3
        }
      ],
      "total_recipes":3,
      "links":[
           "http://localhost:8080/recipes?page=1",
           "http://localhost:8080/recipes?page=3"
      ]
    }
    """

  @recipes @search
  Scenario: search recipes by params vegetarian (name,prep_time,vegetarian)
    Given my system has "recipe":
      | id | name         | prep_time | difficulty | vegetarian |
      | 1  | pizza        | 1232      | 3          | true       |
      | 2  | pizza salami | 1234      | 2          | false      |
    And they have rates:
      | recipe_id | rates   |
      | 1         | 2,3,4,6 |
      | 2         | 1,2,4,6 |
    When I send a GET request to "/recipes?vegetarian=false"
    Then the response code should be 200
    And the response should contain json:
    """
    {
      "data": [
        {
            "id": 2,
            "name": "pizza salami",
            "prep_time": 1234,
            "difficulty":2,
            "vegetarian": false,
            "rate":3.3
        }
      ],
      "total_recipes":2
    }
    """

  @recipes @search
  Scenario: search recipes by params name(name,prep_time,vegetarian)
    Given my system has "recipe":
      | id | name         | prep_time | difficulty | vegetarian |
      | 1  | pizza        | 1232      | 3          | true       |
      | 2  | pizza salami | 1234      | 2          | false      |
    And they have rates:
      | recipe_id | rates   |
      | 1         | 2,3,4,6 |
      | 2         | 1,2,4,6 |
    When I send a GET request to "/recipes?name=pizza salami"
    Then the response code should be 200
    And the response should contain json:
    """
    {
      "data": [
        {
            "id": 2,
            "name": "pizza salami",
            "prep_time": 1234,
            "difficulty":2,
            "vegetarian": false,
            "rate":3.3
        }
      ],
      "total_recipes":2
    }
    """

  @recipes @search
  Scenario: search recipes by params pizza with wildcard(name,prep_time,vegetarian)
    Given my system has "recipe":
      | id | name         | prep_time | difficulty | vegetarian |
      | 1  | pizza        | 1232      | 3          | true       |
      | 2  | pizza salami | 1234      | 2          | false      |
    And they have rates:
      | recipe_id | rates   |
      | 1         | 2,3,4,6 |
      | 2         | 1,2,4,6 |
    When I send a GET request to "/recipes?name=pizza"
    Then the response code should be 200
    And the response should contain json:
    """
    {
      "data": [
        {
            "id": 1,
            "name": "pizza",
            "prep_time": 1232,
            "difficulty":3,
            "vegetarian": true,
            "rate":3.8
        },
        {
            "id": 2,
            "name": "pizza salami",
            "prep_time": 1234,
            "difficulty":2,
            "vegetarian": false,
            "rate":3.3
        }
      ],
      "total_recipes":2
    }
    """

  @recipes @get_by_id
  Scenario: Retrieves a recipe by ID
    Given my system has "recipe":
      | id | name         | prep_time | difficulty | vegetarian |
      | 1  | pizza        | 1232      | 3          | true       |
      | 2  | pizza salami | 1234      | 2          | false      |
    And they have rates:
      | recipe_id | rates   |
      | 1         | 2,3,4,6 |
      | 2         | 1,2,4,6 |
    When I send a GET request to "/recipes/2"
    Then the response code should be 200
    And the response should contain json:
    """
    {
      "data":{
            "id": 2,
            "name": "pizza salami",
            "prep_time": 1234,
            "difficulty":2,
            "vegetarian": false,
            "rate":3.3
      }
    }
    """

  @recipes @update
  Scenario: Update a recipe through
    Given I set header "Authorization" with value "Bearer: 424134134"
    And my system has "recipe":
      | id | name  | prep_time | difficulty | vegetarian |
      | 1  | pizza | 1232      | 3          | true       |
    And they have rates:
      | recipe_id | rates   |
      | 1         | 2,3,4,6 |
    When I send a PATCH request to "/recipes/1" with body:
    """
    {
        "name": "pizza2",
        "prep_time": "1235",
        "difficulty":3,
        "vegetarian": "false"
    }
    """
    Then the response code should be 204
    And the update recipe looks like:
    """
    {
          "id": 1,
          "name": "pizza2",
          "prep_time": 1235,
          "difficulty":3,
          "vegetarian": false,
    }
    """

  @recipes @delete
  Scenario: Delete a recipe through
    Given I set header "Authorization" with value "Bearer: 424134134"
    And my system has "recipe":
      | id | name         | prep_time | difficulty | vegetarian |
      | 1  | pizza        | 1232      | 3          | true       |
      | 2  | pizza salami | 1234      | 2          | false      |
    And they have rates:
      | recipe_id | rates   |
      | 1         | 2,3,4,6 |
      | 2         | 1,2,4,6 |
    When I send a DELETE request to "/recipes/1"
    Then the response code should be 204
    And I will have 0 "recipes" in my system with "id" 1
    And I will have 0 "rates" in my system with "recipe_id" 1


<?php

require_once __DIR__.'/../vendor/autoload.php';

$dependencies = \RecipeService\SharedContext\DI\DependencyConfig::getDependencies();
$routeConfig = \RecipeService\SharedContext\Router\RouteConfig::getRoutes();
$strategy = new \League\Route\Strategy\JsonStrategy();

$router = \RecipeService\SharedContext\Kernel\Kernel::init(
    $dependencies,
    $routeConfig,
    $strategy
);

$router->dispatch();
